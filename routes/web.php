<?php

use App\Http\Controllers\Admin\CampusController;
use App\Http\Controllers\Admin\CarruselController;
use App\Http\Controllers\Admin\CompeticionController;
use App\Http\Controllers\Admin\FichaJugadorController;
use App\Http\Controllers\Admin\GaleriaNavidadController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\Info_CampusController;
use App\Http\Controllers\Admin\Info_NavidadController;
use App\Http\Controllers\Admin\Info_ReservaController;
use App\Http\Controllers\Admin\NoticiasController;
use App\Http\Controllers\Admin\PatrocinadorController;
use App\Http\Controllers\Admin\ReservasController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\MasterController;
use App\Models\Inscripcion_Campus;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/historia', function () {
    return view('tetir.historia');
})->middleware('language');

Route::get('/biografia', function () {
    return view('tetir.juan');
})->middleware('language');

Route::get('/email', function () {
    $data = Inscripcion_Campus::find(11);
    return view('Inscripcion_Campus_PDF', compact('data'));
})->middleware('language');

Route::get('/', [MasterController::class, 'home'])->middleware('language');
Route::get('/campus', [MasterController::class, 'campus'])->middleware('language');

Route::get('/contactanos', [MasterController::class, 'contactanos'])->middleware('language')->name('contacto');
Route::post('/contactanos', [MasterController::class, 'contactanosStore'])->middleware('language')->name('contacto.store');;

Route::get('/ficha-jugador', [MasterController::class, 'fichaJugador'])->middleware('language')->name('ficha');
Route::get('/formulario-campus', [MasterController::class, 'formularioCampus'])->middleware('language')->name('insCampus');
Route::get('/noticia/{noticia}', [MasterController::class, 'noticia'])->middleware('language');
Route::get('/noticias', [MasterController::class, 'noticias'])->middleware('language');
Route::get('/reserva', [MasterController::class, 'reserva'])->middleware('language')->name('reserva');
Route::get('/torneo-navidad', [MasterController::class, 'torneoNavidad'])->middleware('language');

Auth::routes();

// Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('language');

//->middleware('language')



// Administración
Route::group(['middleware' => 'auth'], function () {

    Route::get('admin', [HomeController::class, 'index']);

    //Usuarios
    Route::resource('/admin/users', UsersController::class);
    //Noticias
    Route::resource('/admin/noticias', NoticiasController::class)->middleware('language');

    //Galeria
    Route::resource('/admin/carrusel', CarruselController::class)->middleware('language');
    Route::resource('/admin/patrocinadores', PatrocinadorController::class)->middleware('language');
    Route::resource('/admin/galerianavidad', GaleriaNavidadController::class)->middleware('language');

    //Información
    Route::resource('/admin/infonavidad', Info_NavidadController::class)->middleware('language');
    Route::resource('/admin/infocampus', Info_CampusController::class)->middleware('language');
    Route::resource('/admin/inforeserva', Info_ReservaController::class)->middleware('language');

    //Clasificacionesc
    Route::resource('/admin/competicion', CompeticionController::class)->middleware('language');

});

    //Inscripciones
    Route::resource('/admin/reservas', ReservasController::class)->middleware('language');
    Route::resource('/admin/fichasjugador', FichaJugadorController::class)->middleware('language');
    Route::resource('/admin/inscripcionescampus', CampusController::class)->middleware('language');