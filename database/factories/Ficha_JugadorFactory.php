<?php

namespace Database\Factories;

use App\Models\Ficha_Jugador;
use App\Models\FJ_Datos_Tutores;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class Ficha_JugadorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Ficha_Jugador::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'dni' => $this->faker->phoneNumber(),
            'nombre' => $this->faker->unique()->name(),
            'apellidos' => $this->faker->unique()->sentence(),
            'talla_camisa_polo' => $this->faker->randomElement(['XL', 'L', 'M', 'S', '12-14', '8-10', '4-6']),
            'talla_pantalon' => $this->faker->randomElement(['XL', 'L', 'M', 'S', '14', '12', '10', '8', '6', '4']),
            'licencia' => $this->faker->randomElement(['Benjamín','Alevín','Infantil','Delegado','Entrenador']),
            'fecha_nacimiento' => now(),
            'club_anterior' => $this->faker->name(),
            'nacionalidad' => $this->faker->city(),
            'domicilio' => $this->faker->city(),
            'localidad' => $this->faker->country(),
            'codigo_postal' => $this->faker->randomDigit(),
            'salud' => $this->faker->text(40),
            'user_id' => User::all()->random()->id,
            // 'fj_datos_id' => FJ_Datos_Tutores::all()->random()->id
        ];
    }
}
