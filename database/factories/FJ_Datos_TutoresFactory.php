<?php

namespace Database\Factories;

use App\Models\Ficha_Jugador;
use App\Models\FJ_Datos_Tutores;
use Illuminate\Database\Eloquent\Factories\Factory;

class FJ_Datos_TutoresFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FJ_Datos_Tutores::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'dni' => $this->faker->randomDigit(),
            'nombre' => $this->faker->unique()->name(),
            'apellidos' => $this->faker->unique()->sentence(),
            'fecha_nacimiento' => now(),
            'pais_nacimiento' => $this->faker->country(),
            'nacionalidad' => $this->faker->city(),
            'movil' => $this->faker->randomDigit(),
            'email' => $this->faker->unique()->safeEmail(),
            'ficha__jugador_id' => Ficha_Jugador::all()->random()->id
        ];
    }
}
