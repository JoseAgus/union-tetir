<?php

namespace Database\Factories;

use App\Models\Contacto;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ContactoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Contacto::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [ 
            'nombre' => $this->faker->unique()->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'texto' => $this->faker->text(400),
            'user_id' => User::all()->random()->id
        ];
    }
}
