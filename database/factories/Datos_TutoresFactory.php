<?php

namespace Database\Factories;

use App\Models\Datos_Tutores;
use App\Models\Inscripcion_Campus;
use Illuminate\Database\Eloquent\Factories\Factory;

class Datos_TutoresFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Datos_Tutores::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'dni' => $this->faker->randomDigit(),
            'nombre' => $this->faker->unique()->name(),
            'apellidos' => $this->faker->unique()->sentence(),
            'direccion' => $this->faker->address(),
            'localidad' => $this->faker->country(),
            'email' => $this->faker->unique()->safeEmail(),
            'telf_trabajo' => $this->faker->randomDigit(),
            'movil' => $this->faker->randomDigit(),
            'parentesco' => $this->faker->unique()->name(),
            'ins_campus_id' => Inscripcion_Campus::all()->random()->id
        ];
    }
}
