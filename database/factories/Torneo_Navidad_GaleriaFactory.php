<?php

namespace Database\Factories;

use App\Models\Torneo_Navidad_Galeria;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class Torneo_Navidad_GaleriaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Torneo_Navidad_Galeria::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->unique()->name(),
            'user_id' => User::all()->random()->id
        ];
    }
}
