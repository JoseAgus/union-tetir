<?php

namespace Database\Factories;

use App\Models\User;
use App\Models\Inscripcion_Campus;
use Illuminate\Database\Eloquent\Factories\Factory;

class Inscripcion_CampusFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Inscripcion_Campus::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'dni' => $this->faker->randomDigit(),
            'nombre' => $this->faker->name(),
            'apellidos' => $this->faker->unique()->sentence(),
            'fecha_nacimiento' => now(),
            'domicilio' => $this->faker->address(),
            'club_perteneciente' => $this->faker->name(),
            'talla' => $this->faker->randomElement(['L', 'M', 'S', '16', '12', '10', '8', '4']),
            'categoria' => $this->faker->randomElement(['Prebenjamín','Benjamín','Alevín','Infantil','Cadete','Juvenil']),
            'posicion_juego' => $this->faker->randomElement(['Portero','Defensa','Medio Centro','Delantero']),
            'sexo' => $this->faker->randomElement(['varon', 'mujer']),
            'user_id' => User::all()->random()->id
        ];
    }
}
