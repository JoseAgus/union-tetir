<?php

namespace Database\Factories;

use App\Models\Torneo_Navidad;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class Torneo_NavidadFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Torneo_Navidad::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'titulo' => $this->faker->unique()->sentence(),
            'texto' => $this->faker->text(4000),
            'user_id' => User::all()->random()->id
        ];
    }
}
