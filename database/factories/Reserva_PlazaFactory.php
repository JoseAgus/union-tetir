<?php

namespace Database\Factories;

use App\Models\Reserva_Plaza;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class Reserva_PlazaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Reserva_Plaza::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->unique()->name(),
            'telf_tutor' => $this->faker->randomDigit(),
            'fecha_nacimiento' => now(),
            'obsevaciones' => $this->faker->text(400),
            'user_id' => User::all()->random()->id
        ];
    }
}
