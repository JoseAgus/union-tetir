<?php

namespace Database\Factories;

use App\Models\Carrusel;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class CarruselFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Carrusel::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->unique()->name(),
            'estado' => $this->faker->randomElement([1,2]),
            'user_id' => User::all()->random()->id
        ];
    }
}
