<?php

namespace Database\Factories;

use App\Models\Noticia;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class NoticiaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Noticia::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'titulo' => $this->faker->unique()->sentence(),
            'descripcion' => $this->faker->text(40),
            'texto' => $this->faker->text(4000),
            'estado' => $this->faker->randomElement([1,2]),
            'user_id' => User::all()->random()->id
        ];
    }
}
