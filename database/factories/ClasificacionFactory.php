<?php

namespace Database\Factories;

use App\Models\Categoria;
use App\Models\Clasificacion;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClasificacionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Clasificacion::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre_club' => $this->faker->unique()->name(),
            'puntos_totales' => $this->faker->randomDigit(),
            'partidos_ganados' => $this->faker->randomDigit(),
            'partidos_empatados' => $this->faker->randomDigit(),
            'partidos_perdidos' => $this->faker->randomDigit(),
            'goles_a_favor' => $this->faker->randomDigit(),
            'goles_en_contra' => $this->faker->randomDigit(),
            'categoria_id' => Categoria::all()->random()->id
        ];
    }
}
