<?php

namespace Database\Seeders;

use App\Models\Campus;
use App\Models\Carrusel;
use App\Models\Categoria;
use App\Models\Clasificacion;
use App\Models\Contacto;
use App\Models\Datos_Tutores;
use App\Models\Ficha_Jugador;
use App\Models\FJ_Datos_Tutores;
use App\Models\Info_Reserva;
use App\Models\Inscripcion_Campus;
use App\Models\Noticia;
use App\Models\Patrocinador;
use App\Models\Reserva_Plaza;
use App\Models\Torneo_Navidad;
use App\Models\User;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(1)->create();
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CarruselSeeder::class);
        $this->call(FichaJugadorSeeder::class);
        $this->call(PatrocitadorSeeder::class);
        $this->call(ReservaPlazaSeeder::class);
        $this->call(NoticiaSeeder::class);
        $this->call(TorneoNavidadGaleriaSeeder::class);
        Campus::factory(1)->create();
        Categoria::factory(1)->create();
        Clasificacion::factory(4)->create();
        Inscripcion_Campus::factory(1)->create();
        Datos_Tutores::factory(1)->create();
        FJ_Datos_Tutores::factory(1)->create();
        Info_Reserva::factory(1)->create();
        Torneo_Navidad::factory(1)->create();
    }
}
