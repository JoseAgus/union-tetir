<?php

namespace Database\Seeders;

use App\Models\Imagen;
use App\Models\Reserva_Plaza;
use Illuminate\Database\Seeder;

class ReservaPlazaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $reservaPlazas = Reserva_Plaza::factory(1)->create();

        foreach($reservaPlazas as $reservaPlaza){
            Imagen::factory(1)->create([
                'imageable_id' => $reservaPlaza->id,
                'imageable_type' => Reserva_Plaza::class
            ]);
        }
    }
}
