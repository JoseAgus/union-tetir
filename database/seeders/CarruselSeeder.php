<?php

namespace Database\Seeders;

use App\Models\Carrusel;
use App\Models\Imagen;
use Database\Factories\ImagenFactory;
use Illuminate\Database\Seeder;

class CarruselSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $carrusels = Carrusel::factory(3)->create();

        foreach($carrusels as $carrusel){
            Imagen::factory(1)->create([
                'imageable_id' => $carrusel->id,
                'imageable_type' => Carrusel::class
            ]);
        }
    }
}
