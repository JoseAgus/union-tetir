<?php

namespace Database\Seeders;

use App\Models\Imagen;
use App\Models\Noticia;
use Illuminate\Database\Seeder;

class NoticiaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $notici = Noticia::create([
            'titulo'=>'Jose',
            'descripcion'=>'josejose.com',
            'texto'=> 'ssssssssssssssssssssssssssssssssssssssssssssssssssss',
            'estado'=> 2,
            'user_id' => 1
        ]);
        Imagen::factory(1)->create([
            'imageable_id' => $notici->id,
            'imageable_type' => Noticia::class
        ]);
    }
}
