<?php

namespace Database\Seeders;

use App\Models\Imagen;
use App\Models\Patrocinador;
use Illuminate\Database\Seeder;

class PatrocitadorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $patrocinadores = Patrocinador::factory(3)->create();

        foreach($patrocinadores as $patrocinadore){
            Imagen::factory(1)->create([
                'imageable_id' => $patrocinadore->id,
                'imageable_type' => Patrocinador::class
            ]);
        }
    }
}
