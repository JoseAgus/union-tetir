<?php

namespace Database\Seeders;

use App\Models\Imagen;
use App\Models\Torneo_Navidad_Galeria;
use Illuminate\Database\Seeder;

class TorneoNavidadGaleriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $galeriaNavidad = Torneo_Navidad_Galeria::factory(1)->create();

        foreach($galeriaNavidad as $galeria){
            Imagen::factory(1)->create([
                'imageable_id' => $galeria->id,
                'imageable_type' => Torneo_Navidad_Galeria::class
            ]);
        }
    }
}
