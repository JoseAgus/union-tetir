<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>'Jose',
            'email'=>'jose@jose.com',
            'password'=> bcrypt('abc1234')
        ])->assignRole('Admin');

        User::create([
            'name'=>'Pepe',
            'email'=>'pepe@pepes.com',
            'password'=> bcrypt('abc1234')
        ])->assignRole('Noticiero');
    }
}
