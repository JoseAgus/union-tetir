<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['name' => 'Admin']);
        Role::create(['name' => 'Noticiero']);
 
        Permission::create(['name' => 'solo.admin'])->syncRoles(['Admin']);
        Permission::create(['name' => 'solo.admin.noticia'])->syncRoles(['Admin', 'Noticiero']);


        //Permisos Noticias
        Permission::create(['name' => 'admin.noticia.index'])->syncRoles(['Admin', 'Noticiero']);
        Permission::create(['name' => 'admin.noticia.create'])->syncRoles(['Admin', 'Noticiero']);
    }
}
