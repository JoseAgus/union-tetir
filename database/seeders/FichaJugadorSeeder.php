<?php

namespace Database\Seeders;

use App\Models\Ficha_Jugador;
use App\Models\Imagen;
use Illuminate\Database\Seeder;

class FichaJugadorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fichas = Ficha_Jugador::factory(1)->create();

        foreach($fichas as $ficha){
            Imagen::factory(1)->create([
                'imageable_id' => $ficha->id,
                'imageable_type' => Ficha_Jugador::class
            ]);
        }
    }
}
