<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFichaJugadorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ficha__jugadors', function (Blueprint $table) {
            $table->id();
            $table->string('dni');
            $table->string('nombre');
            $table->string('apellidos');
            $table->enum('talla_camisa_polo',['XL', 'L', 'M', 'S', '12-14', '8-10', '4-6']);
            $table->enum('talla_pantalon',['XL', 'L', 'M', 'S', '14', '12', '10', '8', '6', '4']);
            $table->enum('licencia',['Benjamín','Alevín','Infantil','Delegado','Entrenador']);
            $table->date('fecha_nacimiento');
            $table->string('nacionalidad');
            $table->string('club_anterior');
            $table->string('domicilio');
            $table->string('localidad');
            $table->integer('codigo_postal');
            $table->string('salud');

            $table->unsignedBigInteger('user_id')->unsigned()->default(1);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            // $table->unsignedBigInteger('fj_datos_id')->unsigned();

            // $table->foreign('fj_datos_id')->references('id')->on('f_j__datos__tutores')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ficha__jugadors');
    }
}
