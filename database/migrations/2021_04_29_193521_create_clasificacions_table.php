<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClasificacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clasificacions', function (Blueprint $table) {
            $table->id();
            $table->string('nombre_club');
            $table->integer('puntos_totales');
            $table->integer('partidos_ganados');
            $table->integer('partidos_empatados');
            $table->integer('partidos_perdidos');
            $table->integer('goles_a_favor');
            $table->integer('goles_en_contra');

            $table->unsignedBigInteger('categoria_id')->unsigned();

            $table->foreign('categoria_id')->references('id')->on('categorias')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clasificacions');
    }
}
