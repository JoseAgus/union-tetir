<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservaPlazasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reserva__plazas', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->integer('telf_tutor');
            $table->date('fecha_nacimiento');
            $table->text('obsevaciones');

            $table->unsignedBigInteger('user_id')->unsigned()->default(1);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reserva_plazas');
    }
}
