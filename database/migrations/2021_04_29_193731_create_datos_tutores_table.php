<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDatosTutoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datos__tutores', function (Blueprint $table) {
            $table->id();
            $table->string('dni');
            $table->string('nombre');
            $table->string('apellidos');
            $table->string('direccion');
            $table->string('localidad');
            $table->string('email');
            $table->integer('telf_trabajo');
            $table->integer('movil');
            $table->string('parentesco');

            $table->unsignedBigInteger('ins_campus_id')->unsigned();

            $table->foreign('ins_campus_id')->references('id')->on('inscripcion__campuses')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datos__tutores');
    }
}
