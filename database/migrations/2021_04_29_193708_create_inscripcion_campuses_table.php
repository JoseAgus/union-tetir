<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInscripcionCampusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inscripcion__campuses', function (Blueprint $table) {
            $table->id();
            $table->string('dni');
            $table->string('nombre');
            $table->string('apellidos');
            $table->date('fecha_nacimiento');
            $table->string('domicilio');
            $table->string('club_perteneciente');
            $table->enum('talla',['L', 'M', 'S', '16', '12', '10', '8', '4']);
            $table->enum('categoria',['Prebenjamín','Benjamín','Alevín','Infantil','Cadete','Juvenil']);
            $table->enum('posicion_juego',['Portero','Defensa','Medio Centro','Delantero']);
            $table->enum('sexo',['varon', 'mujer']);

            $table->unsignedBigInteger('user_id')->unsigned()->default(1);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inscripcion__campuses');
    }
}
