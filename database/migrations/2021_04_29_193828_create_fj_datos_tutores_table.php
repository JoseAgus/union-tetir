<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFJDatosTutoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('f_j__datos__tutores', function (Blueprint $table) {
            $table->id();
            $table->string('dni')->nullable();
            $table->string('nombre')->nullable();
            $table->string('apellidos')->nullable();
            $table->string('pais_nacimiento')->nullable();
            $table->date('fecha_nacimiento')->nullable();
            $table->string('nacionalidad')->nullable();
            $table->integer('movil')->nullable();
            $table->string('email')->nullable();

            $table->unsignedBigInteger('ficha__jugador_id')->unsigned()->nullable();

            $table->foreign('ficha__jugador_id')->references('id')->on('ficha__jugadors')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('f_j_datos_tutores');
    }
}
