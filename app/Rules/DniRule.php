<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Symfony\Component\Console\Input\Input;

class DniRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return ($this->isValidDni($value));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Debe ser un Dni válido';
    }

    public function isValidDni($dni)
    {
        $Validni = strtoupper($dni);
        $dniRegEx = '/^[0-9]{8}[A-Z]$/i';
        $letras = "TRWAGMYFPDXBNJZSQVHLCKE";
 
        if (preg_match($dniRegEx, $Validni)) {
            return ($letras[(substr($Validni, 0, 8) % 23)] == $Validni[8]);
        }
 
        return false;
    }
}
