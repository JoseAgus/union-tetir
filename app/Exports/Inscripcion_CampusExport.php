<?php

namespace App\Exports;

use App\Models\Inscripcion_Campus;
use Maatwebsite\Excel\Concerns\FromCollection;

class Inscripcion_CampusExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Inscripcion_Campus::all();
    }
}
