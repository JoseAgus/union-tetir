<?php

namespace App\Exports;

use App\Models\Ficha_Jugador;
use Maatwebsite\Excel\Concerns\FromCollection;

class FichaJugadoresExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Ficha_Jugador::all();
    }
}
