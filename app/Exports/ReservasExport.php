<?php

namespace App\Exports;

use App\Models\Reserva_Plaza;
use Maatwebsite\Excel\Concerns\FromCollection;

class ReservasExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Reserva_Plaza::all();
    }
}
