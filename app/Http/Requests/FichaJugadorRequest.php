<?php

namespace App\Http\Requests;

use App\Rules\DniRule;
use Illuminate\Foundation\Http\FormRequest;

class FichaJugadorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nombre' => 'required',
            'dni' => ['required',new DniRule],
            'apellidos' => 'required',
            'talla_camisa_polo' => 'required',
            'talla_pantalon' => 'required',
            'licencia' => 'required',
            'fecha_nacimiento' => 'required|date',
            'nacionalidad' => 'required',
            'club_anterior' => '',
            'domicilio' => 'required',
            'localidad' => 'required',
            'codigo_postal' => 'required|integer',
            'file' => 'required|image|max:5120,file',
        ];
        return $rules;
    }
}
