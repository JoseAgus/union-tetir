<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReservaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nombre' => 'required',
            'telf_tutor' => 'required|numeric',
            'fecha_nacimiento' => 'required|date',
            'obsevaciones' => 'required',
            'file' => 'required|max:5120,file'
        ];

        return $rules ;
    }
}
