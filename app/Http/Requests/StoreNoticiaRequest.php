<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreNoticiaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $rules = [
            'titulo' => 'required',
            'estado' => 'required|in:1,2',
            'file' => 'image|max:5120,file'
        ];

        if ($this->estado == 2) {
            $rules = array_merge($rules, [
                'descripcion' => 'required',
                'texto' => 'required',
            ]);
        }

        return $rules ;
    }
}
