<?php

namespace App\Http\Requests;

use App\Rules\DniRule;
use Illuminate\Foundation\Http\FormRequest;

class CampusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'dni' => ['required',new DniRule],
            'nombre' => 'required',
            'apellidos' => 'required',
            'fecha_nacimiento' => 'required|date',
            'domicilio' => 'required',
            'club_perteneciente' => 'required',
            'talla' => 'required',
            'categoria' => 'required',
            'posicion_juego' => 'required',
            'sexo' => 'required',

            'dni_0' => ['required',new DniRule],
            'nombre_0' => 'required',
            'apellidos_0' => 'required',
            'direccion_0' => 'required',
            'localidad_0' => 'required',
            'email_0' => 'required|email',
            'telf_trabajo_0' => 'required|integer',
            'movil_0' => 'required|integer',
            'parentesco_0' => 'required',

            'dni_1' => ['required',new DniRule],
            'nombre_1' => 'required',
            'apellidos_1' => 'required',
            'direccion_1' => 'required',
            'localidad_1' => 'required',
            'email_1' => 'required|email',
            'telf_trabajo_1' => 'required|integer',
            'movil_1' => 'required|integer',
            'parentesco_1' => 'required',

        ];
        return $rules;
    }
}
