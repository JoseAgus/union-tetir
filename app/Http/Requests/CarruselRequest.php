<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarruselRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nombre' => 'required',
            'estado' => 'required|in:1,2',
            'file' => 'image|max:5120,file'
        ];
        return $rules ;
    }
}
