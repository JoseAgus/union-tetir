<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\GaleriaNavidadRequest;
use App\Models\Torneo_Navidad_Galeria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class GaleriaNavidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $texto = trim($request->get('texto'));
        $galeria = Torneo_Navidad_Galeria::where('nombre', 'LIKE','%'.$texto.'%')->orderBy('created_at', 'desc')->paginate(10);
        return view('admin.galerias.galerianavidad.index', compact('galeria', 'texto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.galerias.galerianavidad.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GaleriaNavidadRequest $request)
    {
        $galeria = Torneo_Navidad_Galeria::create($request->all());
        if ($request->file('file')) {
            $url = Storage::put('img',$request->file('file'));

            $galeria->image()->create([
                'url' => $url
            ]);
        }

        return redirect()->route('galerianavidad.index', $galeria)->with('create','Imagen Creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Torneo_Navidad_Galeria $galerianavidad)
    {
        return view('admin.galerias.galerianavidad.edit', compact('galerianavidad'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(GaleriaNavidadRequest $request, Torneo_Navidad_Galeria $galerianavidad)
    {
        $galerianavidad->update($request->all());

        if ($request->file('file')) {
            $url = Storage::put('img',$request->file('file'));

            if ($galerianavidad->image) {
                Storage::delete($galerianavidad->image->url);
                $galerianavidad->image()->update([
                    'url' => $url
                ]);
            }else{
                $galerianavidad->image()->create([
                    'url' => $url
                ]);
            }
        }

        return redirect()->route('galerianavidad.index', $galerianavidad)->with('info','Imagen Actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Torneo_Navidad_Galeria $galerianavidad)
    {
        $galerianavidad->delete();

        return redirect()->route('galerianavidad.index')->with('destroy','Imagen Borrada');
    }
}
