<?php

namespace App\Http\Controllers\Admin;

use App\Exports\Inscripcion_CampusExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\CampusRequest;
use App\Models\Datos_Tutores;
use App\Models\Inscripcion_Campus;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;

class CampusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $texto = trim($request->get('texto'));
        $campus = Inscripcion_Campus::where('nombre', 'LIKE','%'.$texto.'%')->orWhere('dni', 'LIKE','%'.$texto.'%')->orderBy('created_at', 'desc')->paginate(10);
        return view('admin.inscripciones.campus.index', compact('campus', 'texto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Excel::download(new Inscripcion_CampusExport, 'Inscripcion_Campus.xlsx');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CampusRequest $request)
    {
        $campus = new Inscripcion_Campus();
        $datos_0 = new Datos_Tutores();
        $datos_1 = new Datos_Tutores();

        $campus->dni =  $request->get('dni');
        $campus->nombre =  $request->get('nombre');
        $campus->apellidos =  $request->get('apellidos');
        $campus->fecha_nacimiento =  $request->get('fecha_nacimiento');
        $campus->domicilio =  $request->get('domicilio');
        $campus->club_perteneciente =  $request->get('club_perteneciente');
        $campus->talla =  $request->get('talla');
        $campus->categoria =  $request->get('categoria');
        $campus->posicion_juego =  $request->get('posicion_juego');
        $campus->sexo =  $request->get('sexo');
        $campus->save();

        //Dato Tutor 0
        $datos_0->dni = $request->get('dni_0');
        $datos_0->nombre = $request->get('nombre_0');
        $datos_0->apellidos = $request->get('apellidos_0');
        $datos_0->direccion = $request->get('direccion_0');
        $datos_0->localidad = $request->get('localidad_0');
        $datos_0->email = $request->get('email_0');
        $datos_0->telf_trabajo = $request->get('telf_trabajo_0');
        $datos_0->movil = $request->get('movil_0');
        $datos_0->parentesco = $request->get('parentesco_0');
        $datos_0->ins_campus_id = $campus->id;
        $datos_0->save();
        
        //Dato Tutor 1
        $datos_1->dni = $request->get('dni_1');
        $datos_1->nombre = $request->get('nombre_1');
        $datos_1->apellidos = $request->get('apellidos_1');
        $datos_1->direccion = $request->get('direccion_1');
        $datos_1->localidad = $request->get('localidad_1');
        $datos_1->email = $request->get('email_1');
        $datos_1->telf_trabajo = $request->get('telf_trabajo_1');
        $datos_1->movil = $request->get('movil_1');
        $datos_1->parentesco = $request->get('parentesco_1');
        $datos_1->ins_campus_id = $campus->id;
        $datos_1->save();
        

        return redirect()->route('insCampus', $campus)->with('create','Se ha enviado la Inscripción al Campus');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Inscripcion_Campus $inscripcionescampus)
    {
        $data = $inscripcionescampus;
        $pdf = PDF::loadView('Inscripcion_Campus_PDF', compact('data'));
        $pdfD = $pdf->download('Inscripcion_Campus.pdf');

        return $pdfD;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inscripcion_Campus $inscripcionescampus)
    {
        $inscripcionescampus->delete();

        return redirect()->route('inscripcionescampus.index')->with('destroy','Inscripción Borrada');
    }
}
