<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Noticia;
use App\Http\Requests\StoreNoticiaRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class NoticiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $texto = trim($request->get('texto'));
        $noticia = Noticia::where('titulo', 'LIKE','%'.$texto.'%')->orWhere('descripcion', 'LIKE','%'.$texto.'%')->orderBy('created_at', 'desc')->paginate(10);
        return view('admin.noticia.index', compact('noticia', 'texto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.noticia.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNoticiaRequest $request)
    {
        
        $noticia = Noticia::create($request->all());
        if ($request->file('file')) {
            $url = Storage::put('img',$request->file('file'));

            $noticia->image()->create([
                'url' => $url
            ]);
        }

        return redirect()->route('noticias.index', $noticia)->with('create','Noticia Creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Noticia $noticia)
    {

        return view('admin.noticia.show', compact('noticia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Noticia $noticia)
    {
        return view('admin.noticia.edit', compact('noticia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreNoticiaRequest $request, Noticia $noticia)
    {
        $noticia->update($request->all());

        if ($request->file('file')) {
            $url = Storage::put('img',$request->file('file'));

            if ($noticia->image) {
                Storage::delete($noticia->image->url);
                $noticia->image()->update([
                    'url' => $url
                ]);
            }else{
                $noticia->image()->create([
                    'url' => $url
                ]);
            }
        }

        return redirect()->route('noticias.index', $noticia)->with('info','Noticia Actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Noticia $noticia)
    {
        $noticia->delete();

        return redirect()->route('noticias.index')->with('destroy','Noticia Borrada');
    }
}
