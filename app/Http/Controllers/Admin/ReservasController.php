<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ReservasExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReservaRequest;
use App\Models\Reserva_Plaza;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;

class ReservasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $texto = trim($request->get('texto'));
        $reserva = Reserva_Plaza::where('nombre', 'LIKE','%'.$texto.'%')->orderBy('created_at', 'desc')->paginate(10);
        return view('admin.inscripciones.reserva.index', compact('reserva', 'texto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Excel::download(new ReservasExport, 'Reservas.xlsx');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReservaRequest $request)
    {
        $reserva = Reserva_Plaza::create($request->all());
        if ($request->file('file')) {
            $url = Storage::put('img',$request->file('file'));

            $reserva->image()->create([
                'url' => $url
            ]);
        }

        return redirect()->route('reserva', $reserva)->with('create','Se ha enviado la reserva');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Reserva_Plaza $reserva)
    {
        $data = $reserva;
        $pdf = PDF::loadView('Reserva_PDF', compact('data'));
        $pdfD = $pdf->download('Reserva.pdf');

        return $pdfD;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reserva_Plaza $reserva)
    {
        $reserva->delete();

        return redirect()->route('reservas.index')->with('destroy','Reserva Borrada');
    }
}
