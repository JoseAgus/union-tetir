<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CarruselRequest;
use App\Models\Carrusel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CarruselController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $texto = trim($request->get('texto'));
        $carrusel = Carrusel::where('nombre', 'LIKE','%'.$texto.'%')->orderBy('created_at', 'desc')->paginate(10);
        return view('admin.galerias.carrusel.index', compact('carrusel', 'texto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.galerias.carrusel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarruselRequest $request)
    {
        $carrusel = Carrusel::create($request->all());
        if ($request->file('file')) {
            $url = Storage::put('img',$request->file('file'));

            $carrusel->image()->create([
                'url' => $url
            ]);
        }

        return redirect()->route('carrusel.index', $carrusel)->with('create','Imagen Creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Carrusel $carrusel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Carrusel $carrusel)
    {
        return view('admin.galerias.carrusel.edit', compact('carrusel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CarruselRequest $request, Carrusel $carrusel)
    {
        $carrusel->update($request->all());

        if ($request->file('file')) {
            $url = Storage::put('img',$request->file('file'));

            if ($carrusel->image) {
                Storage::delete($carrusel->image->url);
                $carrusel->image()->update([
                    'url' => $url
                ]);
            }else{
                $carrusel->image()->create([
                    'url' => $url
                ]);
            }
        }

        return redirect()->route('carrusel.index', $carrusel)->with('info','Imagen Actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Carrusel $carrusel)
    {
        $carrusel->delete();

        return redirect()->route('carrusel.index')->with('destroy','Imagen Borrada');
    }
}
