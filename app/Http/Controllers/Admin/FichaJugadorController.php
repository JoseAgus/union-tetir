<?php

namespace App\Http\Controllers\Admin;

use App\Exports\FichaJugadoresExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\FichaJugadorRequest;
use App\Models\Ficha_Jugador;
use App\Models\FJ_Datos_Tutores;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Barryvdh\DomPDF\Facade as PDF;
use Maatwebsite\Excel\Facades\Excel;

class FichaJugadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $texto = trim($request->get('texto'));
        $fichajuador = Ficha_Jugador::where('nombre', 'LIKE','%'.$texto.'%')->orWhere('dni', 'LIKE','%'.$texto.'%')->orderBy('created_at', 'desc')->paginate(10);
        return view('admin.inscripciones.fichajugador.index', compact('fichajuador', 'texto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Excel::download(new FichaJugadoresExport, 'Ficha_Jugador.xlsx');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FichaJugadorRequest $request)
    {
        // fjdatostutores
        // $fichajugador = Ficha_Jugador::create($request->all());
        
        $fichajugador = new Ficha_Jugador();
        $fj_datos = new FJ_Datos_Tutores();

        $fichajugador->nombre =  $request->get('nombre');
        $fichajugador->apellidos =  $request->get('apellidos');
        $fichajugador->dni =  $request->get('dni');
        $fichajugador->talla_camisa_polo =  $request->get('talla_camisa_polo');
        $fichajugador->talla_pantalon =  $request->get('talla_pantalon');
        $fichajugador->licencia =  $request->get('licencia');
        $fichajugador->fecha_nacimiento =  $request->get('fecha_nacimiento');
        $fichajugador->nacionalidad =  $request->get('nacionalidad');
        $fichajugador->club_anterior =  $request->get('club_anterior');
        $fichajugador->domicilio =  $request->get('domicilio');
        $fichajugador->localidad =  $request->get('localidad');
        $fichajugador->codigo_postal =  $request->get('codigo_postal');
        $fichajugador->salud =  $request->get('salud');
        $fichajugador->save();

        //Dato Tutor
        $fj_datos->nombre = $request->get('nombre_fj');
        $fj_datos->apellidos = $request->get('apellidos_fj');
        $fj_datos->dni = $request->get('dni_fj');
        $fj_datos->pais_nacimiento = $request->get('pais_nacimiento_fj');
        $fj_datos->fecha_nacimiento = $request->get('fecha_nacimiento_fj');
        $fj_datos->nacionalidad = $request->get('nacionalidad_fj');
        $fj_datos->movil = $request->get('movil_fj');
        $fj_datos->email = $request->get('email_fj');
        $fj_datos->ficha__jugador_id = $fichajugador->id;
        $fj_datos->save();

        if ($request->file('file')) {
            $url = Storage::put('img',$request->file('file'));

            $fichajugador->image()->create([
                'url' => $url
            ]);
        }

        return redirect()->route('ficha', $fichajugador)->with('create','Se ha enviado la Ficha de Jugador');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Ficha_Jugador $fichasjugador)
    {
        $data = $fichasjugador;
        $pdf = PDF::loadView('FichaJugadores_PDF', compact('data'));
        $pdfD = $pdf->download('Ficha_Jugador.pdf');

        return $pdfD;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ficha_Jugador $fichasjugador)
    {
        $fichasjugador->delete();

        return redirect()->route('fichasjugador.index')->with('destroy','Ficha de Jugador Borrada');
    }
}
