<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PatrocinadorRequest;
use App\Models\Patrocinador;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PatrocinadorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $texto = trim($request->get('texto'));
        $patrocinadore = Patrocinador::where('nombre', 'LIKE','%'.$texto.'%')->orderBy('created_at', 'desc')->paginate(10);
        return view('admin.galerias.patrocinadores.index', compact('patrocinadore', 'texto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.galerias.patrocinadores.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PatrocinadorRequest $request)
    {
        $patrosinadores = Patrocinador::create($request->all());
        if ($request->file('file')) {
            $url = Storage::put('img',$request->file('file'));

            $patrosinadores->image()->create([
                'url' => $url
            ]);
        }

        return redirect()->route('patrocinadores.index', $patrosinadores)->with('create','Patrocinador Creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Patrocinador $patrocinadore)
    {
        return view('admin.galerias.patrocinadores.edit', compact('patrocinadore'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PatrocinadorRequest $request, Patrocinador $patrocinadore)
    {
        $patrocinadore->update($request->all());

        if ($request->file('file')) {
            $url = Storage::put('img',$request->file('file'));

            if ($patrocinadore->image) {
                Storage::delete($patrocinadore->image->url);
                $patrocinadore->image()->update([
                    'url' => $url
                ]);
            }else{
                $patrocinadore->image()->create([
                    'url' => $url
                ]);
            }
        }

        return redirect()->route('patrocinadores.index', $patrocinadore)->with('info','Patrocinador Actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patrocinador $patrocinadore)
    {
        $patrocinadore->delete();

        return redirect()->route('patrocinadores.index')->with('destroy','Patrocinador Borrado');
    }
}
