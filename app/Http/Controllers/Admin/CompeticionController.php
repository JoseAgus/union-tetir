<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompeticionRequest;
use App\Models\Categoria;
use App\Models\Clasificacion;
use Illuminate\Http\Request;

class CompeticionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $texto = trim($request->get('texto'));
        $categoria = Categoria::where('nombre', 'LIKE','%'.$texto.'%')->orderBy('created_at', 'desc')->paginate(1);

        return view('admin.competicion.index', compact('categoria', 'texto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.competicion.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompeticionRequest $request)
    {
        $categoria = new Categoria();

        $categoria->nombre = $request->get('nombre');
        $categoria->nombre_breve =$request->get('nombre_breve');
        $categoria->estado =$request->get('estado');
        $categoria->save();

            foreach ($request->input('club', []) as $i => $club) {
                if ($club == null) {
                    $null = '(Sin Nombre)';
                }else{
                    $null = $club;
                }
                Clasificacion::create([
                    'nombre_club'           => $null,
                    'puntos_totales'        => $request->input('pt.' . $i),
                    'partidos_ganados'      => $request->input('pg.' . $i),
                    'partidos_empatados'    => $request->input('pe.' . $i),
                    'partidos_perdidos'     => $request->input('pp.' . $i),
                    'goles_a_favor'         => $request->input('gf.' . $i),
                    'goles_en_contra'       => $request->input('gc.' . $i),
                    'categoria_id'          => $categoria->id,
                ]);

            }

        return redirect()->route('competicion.index', $categoria)->with('create','Competición Creada');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Categoria $competicion)
    {
        return view('admin.competicion.edit', compact('competicion'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CompeticionRequest $request, Categoria $competicion)
    {
        $competicion->clasificacion()->delete();

        $categoria = $competicion;

        $categoria->nombre = $request->get('nombre');
        $categoria->nombre_breve =$request->get('nombre_breve');
        $categoria->estado =$request->get('estado');
        $categoria->save();

            foreach ($request->input('club', []) as $i => $club) {
                if ($club == null) {
                    $null = '(Sin Nombre)';
                }else{
                    $null = $club;
                }
                Clasificacion::create([
                    'nombre_club'           => $null,
                    'puntos_totales'        => $request->input('pt.' . $i),
                    'partidos_ganados'      => $request->input('pg.' . $i),
                    'partidos_empatados'    => $request->input('pe.' . $i),
                    'partidos_perdidos'     => $request->input('pp.' . $i),
                    'goles_a_favor'         => $request->input('gf.' . $i),
                    'goles_en_contra'       => $request->input('gc.' . $i),
                    'categoria_id'          => $categoria->id,
                ]);

            }

        return redirect()->route('competicion.index', $categoria)->with('info','Competición Actualizada');
        
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categoria $competicion)
    {
        $competicion->delete();

        return redirect()->route('competicion.index')->with('destroy','Competición Borrada');
    }
}
