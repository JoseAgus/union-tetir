<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactoRequest;
use App\Mail\ContactoMaileable;
use App\Models\Campus;
use App\Models\Carrusel;
use App\Models\Categoria;
use App\Models\Contacto;
use App\Models\Ficha_Jugador;
use App\Models\Info_Reserva;
use App\Models\Noticia;
use App\Models\Patrocinador;
use App\Models\Reserva_Plaza;
use App\Models\Torneo_Navidad;
use App\Models\Torneo_Navidad_Galeria;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class MasterController extends Controller
{
    public function home()
    {
        $carusels = Carrusel::where('estado', 2)->get();
        $categoria = Categoria::all();
        $patrocinadores = Patrocinador::all();
        $noticias = Noticia::where('estado', 2)->orderBy('created_at', 'desc')->limit(3)->get();
        $categorias = Categoria::where('estado', 2)->get();
        return view('home', compact('carusels', 'patrocinadores', 'noticias', 'categorias'));
    }
    public function campus()
    {
        $infoCampus = Campus::all();
        return view('tetir.campus', compact('infoCampus'));
    }

    public function contactanos()
    {
        return view('tetir.contactanos');
    }

    public function contactanosStore(ContactoRequest $request)
    {
        $correo = new ContactoMaileable($request->all());

        Mail::to('zellsusj229@gmail.com')->send($correo);
        return redirect()->route('contacto')->with('create','Mensaje Enviado');
        // return 'hola'; 
    }

    public function fichaJugador()
    {
        // Transformando el enum 'talla_camisa_polo' en un Array 
        $type = DB::select(DB::raw("SHOW COLUMNS FROM ficha__jugadors WHERE Field = 'talla_camisa_polo'"))[0]->Type ;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $tallaCamisa = array();
        foreach( explode(',', $matches[1]) as $value )
        {
          $v = trim( $value, "'" );
          $tallaCamisa = Arr::add($tallaCamisa, $v, $v);
        }

        // Transformando el enum 'talla_pantalon' en un Array 
        $type = DB::select(DB::raw("SHOW COLUMNS FROM ficha__jugadors WHERE Field = 'talla_pantalon'"))[0]->Type ;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $tallaPantalon = array();
        foreach( explode(',', $matches[1]) as $value )
        {
            $v = trim( $value, "'" );
            $tallaPantalon = Arr::add($tallaPantalon, $v, $v);
        }
                
        
        // Transformando el enum 'licencia' en un Array 
        $type = DB::select(DB::raw("SHOW COLUMNS FROM ficha__jugadors WHERE Field = 'licencia'"))[0]->Type ;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $licencia = array();
        foreach( explode(',', $matches[1]) as $value )
        {
            $v = trim( $value, "'" );
            $licencia = Arr::add($licencia, $v, $v);
        }

        return view('tetir.ficha-jugador', compact('tallaCamisa', 'tallaPantalon', 'licencia'));
    }

    public function formularioCampus()
    {
        // Transformando el enum 'talla' en un Array 
        $type = DB::select(DB::raw("SHOW COLUMNS FROM inscripcion__campuses WHERE Field = 'talla'"))[0]->Type ;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $talla = array();
        foreach( explode(',', $matches[1]) as $value )
        {
            $v = trim( $value, "'" );
            $talla = Arr::add($talla, $v, $v);
        }

        // Transformando el enum 'categoria' en un Array 
        $type = DB::select(DB::raw("SHOW COLUMNS FROM inscripcion__campuses WHERE Field = 'categoria'"))[0]->Type ;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $categoria = array();
        foreach( explode(',', $matches[1]) as $value )
        {
            $v = trim( $value, "'" );
            $categoria = Arr::add($categoria, $v, $v);
        }

        // Transformando el enum 'posicion_juego' en un Array 
        $type = DB::select(DB::raw("SHOW COLUMNS FROM inscripcion__campuses WHERE Field = 'posicion_juego'"))[0]->Type ;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $posicion_juego = array();
        foreach( explode(',', $matches[1]) as $value )
        {
            $v = trim( $value, "'" );
            $posicion_juego = Arr::add($posicion_juego, $v, $v);
        }

        return view('tetir.formulario-campus', compact('talla', 'categoria', 'posicion_juego'));
    }

    public function noticia(Noticia $noticia)
    {
        return view('tetir.noticia', compact('noticia'));
    }

    public function noticias()
    {
        $noticias = Noticia::where('estado', 2)->orderBy('created_at', 'desc')->paginate(6);
        return view('tetir.noticias', compact('noticias'));
    }

    public function reserva()
    {
        $reserva = Info_Reserva::all();
        return view('tetir.reserva', compact('reserva'));
    }

    public function torneoNavidad()
    {
        $torneoNavidad = Torneo_Navidad::all();
        $galeriaNavidad = Torneo_Navidad_Galeria::paginate(6);
        return view('tetir.torneo-navidad', compact('torneoNavidad', 'galeriaNavidad'));
    }

}
