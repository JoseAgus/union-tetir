<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Datos_Tutores extends Model
{
    use HasFactory;

    //Relacion 1:N de Inscripcion Campus
    public function inscripcioncampu()
    {
        return $this->belongsTo(Inscripcion_Campus::class);
    }
}
