<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ficha_Jugador extends Model
{
    use HasFactory;
    protected $guarded = ['id','created_at','update_at'];
    //Relacion 1:N User
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    //Relacion Polimorfica Imagen
    public function image()
    {
        return $this->morphOne(Imagen::class, 'imageable');
    }

    //Relacion 1:N FJ_Datos_Tutores
    public function fjdatostutores()
    {
        // return $this->belongsTo(FJ_Datos_Tutores::class);
        return $this->hasMany(FJ_Datos_Tutores::class);
    }
}
