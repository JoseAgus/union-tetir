<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clasificacion extends Model
{
    use HasFactory;
    protected $guarded = ['id','created_at','update_at'];
    //Relacion 1:N con Categoria
    public function categoria()
    {
        return $this->belongsTo(Categoria::class);
    }
    // public function categoria()
    // {
    //     return $this->hasMany(Categoria::class);
    // }
}
