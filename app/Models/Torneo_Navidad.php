<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Torneo_Navidad extends Model
{
    use HasFactory;
    protected $guarded = ['id','created_at','update_at'];
    //Relacion 1:1 User
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
