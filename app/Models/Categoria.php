<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    use HasFactory;
    protected $guarded = ['id','created_at','update_at'];
    //Relacion 1:N User
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    //Relacion 1:N Clasificasiones
    public function clasificacion()
    {
        return $this->hasMany(Clasificacion::class);
    }
}
