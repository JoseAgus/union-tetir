<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FJ_Datos_Tutores extends Model
{
    use HasFactory;
    protected $guarded = ['id','created_at','update_at'];
    //Relacion 1:N de Ficha Jugador
    public function fichajugador()
    {
        // return $this->hasMany(Ficha_Jugador::class);
        return $this->belongsTo(Ficha_Jugador::class);
    }
}
