<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inscripcion_Campus extends Model
{
    use HasFactory;

    //Relacion 1:N User
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //Relacion 1:N Datos_Tutores
    public function datostutor()
    {
        return $this->hasMany(Datos_Tutores::class, 'ins_campus_id');
    }
}
