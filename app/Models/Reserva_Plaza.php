<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reserva_Plaza extends Model
{
    use HasFactory;
    protected $guarded = ['id','created_at','update_at'];
    //Relacion 1:N User
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    //Relacion Polimorfica Imagen
    public function image()
    {
        return $this->morphOne(Imagen::class, 'imageable');
    }
}
