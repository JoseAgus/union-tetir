<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable;
    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //Relaciones 1:N Rezerva-Plaza
    public function resevaPlaza()
    {
        return $this->hasMany(Reserva_Plaza::class);
    }
    //Relaciones 1:N Carrusels
    public function carrusel()
    {
        return $this->hasMany(Carrusel::class);
    }
    //Relaciones 1:N Patrocinadores
    public function patrocinadores()
    {
        return $this->hasMany(Patrocinador::class);
    }
    //Relaciones 1:N Categoria
    public function categoria()
    {
        return $this->hasMany(Categoria::class);
    }
    //Relaciones 1:N Contactos
    public function contacto()
    {
        return $this->hasMany(Contacto::class);
    }
    //Relaciones 1:N Inscripciones-Campus
    public function inscripcionesCampus()
    {
        return $this->hasMany(Inscripcion_Campus::class);
    }
    //Relaciones 1:N Ficha-Jugadores
    public function fichaJugadores()
    {
        return $this->hasMany(Ficha_Jugador::class);
    }
    //Relaciones 1:N Noticias
    public function noticia()
    {
        return $this->hasMany(Noticia::class);
    }

    //Relaciones 1:N Torneo_Navidad_Galeria
    public function torneoNavidadGaleria()
    {
        return $this->hasMany(Torneo_Navidad_Galeria::class);
    }
    
    //Relaciones 1:1 Info-Reserva
    public function infoReserva()
    {
        return $this->hasOne(Info_Reserva::class);
    }
    //Relaciones 1:1 Campus
    public function campus()
    {
        return $this->hasOne(Campus::class);
    }
    //Relaciones 1:1 Torneo-Navidad
    public function torneoNavidad()
    {
        return $this->hasOne(Torneo_Navidad::class);
    }
}
