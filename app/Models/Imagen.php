<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Imagen extends Model
{
    use HasFactory;
    protected $fillable = ['url'];
    //Relacion Polimorfica 
    //Carrusels 
    //Patrocinadores 
    //Ficha-Jugador 
    //Reserva-Plazas 
    //Noticia
    public function imageable()
    {
        return $this->morphTo();
    }
}
