<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
        
        <!-- Owl Carousel -->
        <link rel="stylesheet" href="{{ asset('plugins/owl.carousel2/assets/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/owl.carousel2/assets/owl.theme.default.min.css') }}">
    
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.default.css') }}" id="theme-stylesheet">
    
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    
    
    </head>
<body>
        <br>
        <h1>{{ $subject }}</h1>

        <h3>Nombre del Usuario: <b>{{ $contacto['nombre']}}</b></h3>
        <h3>Correo del Usuario: <b>{{ $contacto['email']}}</b></h3>
        <br>
        <h3><b> Mensaje del Usuario: </b></h3>
        <h4>{{ $contacto['mensaje']}}</h4>


</body>
</html>