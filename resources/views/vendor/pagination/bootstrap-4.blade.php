@if ($paginator->hasPages())
    <nav>
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
 
                <div class="col-sm-6 text-center text-sm-left mb-2 mb-sm-0"><a
                        class="page-item btn btn-outline-secondary btn-sm disabled" aria-hidden="true" aria-disabled="true"
                        aria-label="@lang('pagination.previous')"><i class="fas fa-angle-left mr-2"></i>Anterior</a>
                </div>
 
            @else
 
                <div class="col-sm-6 text-center text-sm-left mb-2 mb-sm-0"><a
                        class="btn btn-outline-secondary btn-sm" href="{{ $paginator->previousPageUrl() }}"
                        rel="prev" aria-label="@lang('pagination.previous')"><i class="fas fa-angle-left mr-2"></i>Anterior</a>
                </div>
 
            @endif
 
            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
 
                <div class="col-sm-6 text-center text-sm-right text-right"><a
                        class="btn btn-outline-secondary btn-sm" href="{{ $paginator->nextPageUrl() }}"
                        rel="next" aria-label="@lang('pagination.next')">Siguiente<i
                            class="fas fa-angle-right ml-2"></i></a></div>
 
            @else
 
                <div class="col-sm-6 text-center text-sm-right text-right"><a
                        class="page-item btn btn-outline-secondary btn-sm disabled" aria-hidden="true" aria-disabled="true"
                        aria-label="@lang('pagination.next')">Siguiente<i class="fas fa-angle-right ml-2"></i></a>
                </div>
 
            @endif
        </ul>
    </nav>
@endif