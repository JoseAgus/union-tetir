@extends('layouts.master')

@section('content')
<section>

    <!-- Carrusel -->
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100 py-3" src="img/web_tetir_portada_01.jpg" alt="CampusTetir 2019">
            </div>
            @foreach ($carusels as $carusel)
                <div class="carousel-item">
                    <img class="d-block w-100 py-3" src="{{ asset('storage/'.$carusel->image->url) }}" alt="{{ $carusel->nombre }}">
                   
                </div>
            @endforeach

        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-12 mb-4 mb-lg-0">

                <div class="col-lg-12 text-center">

                    <h1>Noticias</h1>
                    <br>
                </div>

                    <div class="card-columns text-center ">

                        @if (empty($noticias))
                            <h3>No hay Noticias</h3>
                        @else
                            @foreach ($noticias as $noticia)
                            <div class="card" style="width: 13rem;">
                                <a href="{{ url('/noticia/' . $noticia->id)}}">
                                    @if ($noticia->image)
                                    <img src="{{ asset('storage/'.$noticia->image->url) }}" class="card-img-top" alt="{{$noticia->image->url}}">
                                    @else
                                    <img src="https://pixabay.com/get/g5db51d03683eacf82f6f744d45f69ef81896738da7bf87ef2b687f4c82d00fecec291e5032c5f001d8432951a0e9c536_1280.jpg" class="card-img-top" alt="...">
                                    @endif
                                </a>
                                <div class="card-body">
                                    <h5 class="card-title">{{ $noticia->titulo }}</h5>
                                    <p class="card-text">{{ $noticia->descripcion }}</p>
                                </div>

                            </div>
                            @endforeach
                        @endif
                    </div>
                
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 mb-5 mb-lg-0 text-center">
                <br>
                <h2>Clasificaciones</h2>
                <br>
                @if (empty($categorias))
                    <h3>No hay Competiciones en Curso</h3>
                @else
                    <ul class="nav nav-tabs" role="tablist">

                        {{-- Bucle con las Categorias nombre breve--}}
                        {{-- Codigo para el bucle --}}
                        {{-- <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="{{'#L'. $categoria->id}}">Todas Las Clasificasiones</a>
                            
                        </li> --}}
                        @for ($i = 0; $i < count($categorias); $i++)
                                <?php $categoria = $categorias[$i]; ?>

                        <li class="nav-item">
                            <a class="nav-link @if ($i == 0) active @endif" data-toggle="tab" href="{{'#L'. $categoria->id}}">{{ $categoria->nombre_breve }}</a>
                            
                        </li>
                        @endfor
                    </ul>
                        
                    {{-- Fin Codigo para el bucle --}}
                    
                    <div id="Clasificasiones" class="tab-content">
                            
                            {{-- Bucle con las Categorias nombre--}}
                            {{-- Codigo para el bucle --}}
                            @for ($i = 0; $i < count($categorias); $i++)
                                <?php $categoria = $categorias[$i]; ?>

                                <div id="{{'L'.$categoria->id}}" class="container tab-pane @if ($i == 0) active @endif"><br>
                                    <h3>{{$categoria->nombre}}</h3>
                                    @if (empty($categoria->clasificacion()))
                                        <h3>La clasificacion no esta puesta</h3>
                                    @else
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>Club</th>
                                                    <th>P</th>
                                                    <th>PG</th>
                                                    <th>PE</th>
                                                    <th>PP</th>
                                                    <th>GF</th>
                                                    <th>GC</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <!-- tr = una linea -->
                                                {{-- Bucle con las Calificaciones --}}
                                                {{-- Codigo para el bucle --}}
                                                @foreach ($categoria->clasificacion()->orderBy('puntos_totales', 'desc')->get() as $clasificacione)
                                                    <tr>
                                                        <td>{{ $clasificacione->nombre_club }}</td>
                                                        <td>{{ $clasificacione->puntos_totales }}</td>
                                                        <td>{{ $clasificacione->partidos_ganados }}</td>
                                                        <td>{{ $clasificacione->partidos_empatados }}</td>
                                                        <td>{{ $clasificacione->partidos_perdidos }}</td>
                                                        <td>{{ $clasificacione->goles_a_favor }}</td>
                                                        <td>{{ $clasificacione->goles_en_contra }}</td>
                                                    </tr>
                                                @endforeach
                                                {{-- Fin Codigo para el bucle Calificaciones --}}
                                            </tbody>
                                        </table>
                                        <span class="text-muted"><b>P:</b> Puntos <b>PG:</b> Partidos Ganados <b>PE:</b> Partidos Empatados <b>PP:</b> Partidos Perdidos <b>GF</b>: Goles a Favor <b>GC:</b> Goles en Contra </span>
                                    @endif
                                </div>
                            @endfor
                            {{-- Fin Codigo para el bucle Categoria--}}

                    </div>
                        
                @endif
            </div>
        </div>
    </div>
    </div>
</section>

<section class="py-5">
    <div class="container py-4">
        <header class="text-center mb-4">
            <br>
            <h2>Colaboradores y Patrocinadores</h2>
        </header>
    @if (empty($patrocinadores))
        <h3>No hay Patrocinadores</h3>
    @else
        <div class="owl-carousel sponsors-slider">

            {{-- Bucle Con Patrocinadores --}}
            {{-- Codigo para el bucle --}}
            @foreach ($patrocinadores as $patrocinadore)
                <a href=""><img class="d-block mx-auto my-3 sponsor" src="{{ asset('storage/'.$patrocinadore->image->url) }}" alt="{{ $patrocinadore->nombre }}"></a>
            @endforeach
            {{-- Fin Codigo para el bucle --}}
        </div>
    @endif
    </div>
</section>
@endsection 