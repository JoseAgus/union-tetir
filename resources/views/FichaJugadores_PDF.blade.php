<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
        
        <!-- Owl Carousel -->
        <link rel="stylesheet" href="{{ asset('plugins/owl.carousel2/assets/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/owl.carousel2/assets/owl.theme.default.min.css') }}">
    
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.default.css') }}" id="theme-stylesheet">
    
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    
    
    </head>
<body>

    <div class="col-12">

        <a class="" href="https://www.uniontetir.com/"><img class="mb-2" src="img/Escudo-Union-Tetir.png" alt="Asociación del Union Tetir C.F." width="140">
                    
        </a>
        <h1>Ficha De Jugador</h1>
        <div class="text-left col-6 float-left border">
            <h3>Datos del Jugador</h3>
            <b>Dni:</b> {{$data->dni}}
            <br>
            <b>Nombre:</b> {{$data->nombre}}
            <br>
            <b>Apellidos:</b> {{$data->apellidos}}
            <br>
            <b>Talla de Camisa o Polo:</b> {{$data->talla_camisa_polo}}
            <br>
            <b>Talla del Pantalon:</b> {{$data->talla_pantalon}}
            <br>
            <b>Licencia:</b> {{$data->licencia}}
            <br>
            <b>Fecha de Nacimiento:</b> {{$data->fecha_nacimiento}}
            <br>
            <b>Nacionalidad:</b> {{$data->nacionalidad}}
            <br>
            <b>Club Anterior:</b> {{$data->club_anterior}}
            <br>
            <b>Domicilio:</b> {{$data->domicilio}}
            <br>
            <b>Localidad:</b> {{$data->localidad}}
            <br>
            <b>Codigo Postal:</b> {{$data->codigo_postal}}
            <br>
            <b>Intolerancias:</b> {{$data->salud}}
        </div>

        <div class="col-6 text-right float-left">
            <h5>Información</h5>
            <p style="font-size: 12px">Disciplina deportiva: <b>FUTBOL BASE</b> </p>
            <p style="font-size: 12px"><b>V Campus Formativo Unión Tetir CF</b></p>
            <p style="font-size: 12px">Instalación deportiva <b>Domingo J. Vera Alonso</b></p>
            <p style="font-size: 12px">Estadio <b>Municipal de TETIR</b></p>
            <p style="font-size: 12px">Cajamar, Nº CCC: <b>ES7830586118222720017199</b></p>
            <p style="font-size: 12px"><b>Teléfonos Contacto: 629413476,636476777</b></p>
            <p style="font-size: 12px"><b>Email: uniontetircf@hotmail.com</b></p>
            <p style="font-size: 12px"><b><a href="https://www.uniontetir.com/"> www.uniontetir.com </a></b></p>
        </div>

        
        @if (empty($data->fjdatostutores->nombre))
            
        @else
            <div class="text-left col-6 ">
                <h3>Datos Tutores</h3>
            </div>
            @foreach ($data->fjdatostutores as $datas)
        
            <div class="col-5 float-left border">
                <b>Dni:</b> {{$datas->dni}}
                <br>
                <br>
                <b>Nombre:</b> {{$datas->nombre}}
                <br>
                <br>
                <b>Apellidos:</b> {{$datas->apellidos}}
                <br>
                <br>
                <b>Pais De Nacimiento:</b> {{$datas->pais_nacimiento}}
                <br>
                <br>
                <b>Fecha de Nacimiento:</b> {{$datas->fecha_nacimiento}}
                <br>
                <br>
                <b>Nacionalidad:</b> {{$datas->nacionalidad}}
                <br>
                <br>
                <b>Movil:</b> {{$datas->movil}}
                <br>
                <br>
                <b>Correo Electronico:</b> {{$datas->email}}

            </div>
            @endforeach
        @endif
       

    </div>
</body>
</html>