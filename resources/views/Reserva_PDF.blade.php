<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
        
        <!-- Owl Carousel -->
        <link rel="stylesheet" href="{{ asset('plugins/owl.carousel2/assets/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('plugins/owl.carousel2/assets/owl.theme.default.min.css') }}">
    
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/style.default.css') }}" id="theme-stylesheet">
    
        <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
    
    
    </head>
<body>

    <div class="col-12">

        <a class="" href="https://www.uniontetir.com/"><img class="mb-2" src="img/Escudo-Union-Tetir.png" alt="Asociación del Union Tetir C.F." width="140">
                    
        </a>
        <h1>Reserva</h1>
        <div class="text-left col-6 float-left border">
            <h3>Datos de la Reserva</h3>
            <b>Nombre:</b> {{$data->nombre}}
            <br>
            <b>Teléfono Tutor:</b> {{$data->telf_tutor}}
            <br>
            <b>Fecha de Nacimiento:</b> {{$data->fecha_nacimiento}}
            <br>
            <b>Observaciones:</b> {{$data->obsevaciones}}
        </div>

        <div class="col-6 text-right float-left">
            <h5>Información</h5>
            <p style="font-size: 12px">Disciplina deportiva: <b>FUTBOL BASE</b> </p>
            <p style="font-size: 12px"><b>V Campus Formativo Unión Tetir CF</b></p>
            <p style="font-size: 12px">Instalación deportiva <b>Domingo J. Vera Alonso</b></p>
            <p style="font-size: 12px">Estadio <b>Municipal de TETIR</b></p>
            <p style="font-size: 12px">Cajamar, Nº CCC: <b>ES7830586118222720017199</b></p>
            <p style="font-size: 12px"><b>Teléfonos Contacto: 629413476,636476777</b></p>
            <p style="font-size: 12px"><b>Email: uniontetircf@hotmail.com</b></p>
            <p style="font-size: 12px"><b><a href="https://www.uniontetir.com/"> www.uniontetir.com </a></b></p>
        </div>

    </div>
</body>
</html>