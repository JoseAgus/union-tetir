@extends('adminlte::page')

@section('title')

@section('content_header')
    <h1>Usuarios</h1>
@stop

@section('content')

@if (session('info'))
    <div class="alert alert-info">
        <strong>{{ session('info')}}</strong>
    </div>
@endif

@if (session('create'))
    <div class="alert alert-success">
        <strong>{{ session('create')}}</strong>
    </div>
@endif

@if (session('destroy'))
    <div class="alert alert-danger">
        <strong>{{ session('destroy')}}</strong>
    </div>
@endif
    
    <div class="card">
        <div class="card-body">

            <form action="{{ route('users.index')}}" method="GET">
                @csrf
                <div class="form-row">
                    <div class="col-auto">
                        <input type="submit" class="btn btn-info" value="Buscar">
                    </div>
                
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="texto" placeholder="Busque por el Nombre" value="{{$texto}}">
                    </div>
                </div>
            </form>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Correo Electronico</th>
                        <th>Fecha</th>
                        <th colspan="2"></th>
                    </tr>
                </thead>
                
                <tbody>
                    @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->created_at }}</td>
                            
                            <td><a href="{{ route('users.edit',$user)}}" class="btn btn-primary"><i class="fas fa-edit"></i></a></td>
                            <td>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{'#exampleModalCenter'.$user->id}}">
                                    <i class="fas fa-trash"></i>
                                  </button>

                                    <!-- Modal -->
                                        <div class="modal fade" id="{{'exampleModalCenter'.$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">Confirmar Borrado</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        </div>
                                                        <div class="modal-body">
                                                        ¿Está seguro de que quiere borrar la noticia "{{ $user->name }}"?
                                                        </div>
                                                        <div class="modal-footer">
                                                        <button type="button" class="btn btn-success" data-dismiss="modal">Cancelar</button>

                                                        <form action="{{ route('users.destroy', $user)}}" method="POST">
                                                            @csrf
                                                            @method('delete')
                                                            <button type="submit" class="btn btn-danger">Aceptar</button>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <!-- Fin Modal -->
                            </td>
                            
                        </tr>
                        
                    @endforeach
                </tbody>
            </table>
            {{ $users->links() }}
        </div>
    </div>

@stop