@extends('adminlte::page')

@section('title')

@section('content_header')
    <h1>Asigna un Rol</h1>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        <h5>Nombre:</h5>
        <p class="form-control">{{ $user->name}}</p>
        {!! Form::model($user,['route' => ['users.update', $user], 'method' => 'put']) !!}
            @csrf
            {!! Form::label('roles', 'Lista de Roles') !!}
            @foreach ($roles as $rol)
                <div>
                    <label>
                        {!! Form::checkbox('roles[]', $rol->id, null, ['class' => 'mr-1']) !!}
                        {{ $rol->name }}
                    </label>
                </div>
            @endforeach
            {!! Form::submit('Actualizar Usuario', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
    </div>
</div>
@stop

@section('css')
<style>

</style>
@stop

@section('js')
<script src="https://cdn.ckeditor.com/ckeditor5/27.1.0/classic/ckeditor.js"></script>

<script>

    ClassicEditor
    .create( document.querySelector( '#texto' ) )
    .catch( error => {
        console.error( error );
    });

    //Cambiar imagen
    document.getElementById("file").addEventListener('change', cambiarImagen);

    function cambiarImagen(event) {
        var file = event.target.files[0];

        var reader = new FileReader();
        reader.onload = (event) => {
            document.getElementById("picture").setAttribute('src', event.target.result);
        };

        reader.readAsDataURL(file);
    }

</script>
@stop