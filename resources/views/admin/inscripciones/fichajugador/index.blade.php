@extends('adminlte::page')

@section('title')

@section('content_header')
    <h1>Fichas de Jugador</h1>
@stop

@section('content')

@if (session('info'))
    <div class="alert alert-info">
        <strong>{{ session('info')}}</strong>
    </div>
@endif

@if (session('destroy'))
    <div class="alert alert-danger">
        <strong>{{ session('destroy')}}</strong>
    </div>
@endif
    
    <div class="card">
        <div class="card-body">
            <a href="{{ route('fichasjugador.create')}}" class="btn btn-success btn-lg float-right"><i class="fas fa-file-csv"></i></a>
            <form action="{{ route('fichasjugador.index')}}" method="GET">
                @csrf
                <div class="form-row">
                    <div class="col-auto">
                        <input type="submit" class="btn btn-info" value="Buscar">
                    </div>
                
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="texto" placeholder="Busque por Dni o Nombre" value="{{$texto}}">
                    </div>
                </div>
            </form>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Dni</th>
                        <th>Nombre</th>
                        <th>Apellidos</th>
                        <th>Talla Camisa o Polo</th>
                        <th>Talla Pantalon</th>
                        <th>Licencia</th>
                        <th>Posición de Juego</th>
                        <th>Fecha de Nacimiento</th>
                        <th colspan="2"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($fichajuador as $fichajuadors)
                        <tr>
                            <td>{{ $fichajuadors->id }}</td>
                            <td>{{ $fichajuadors->dni }}</td>
                            <td>{{ $fichajuadors->nombre }}</td>
                            <td>{{ $fichajuadors->apellidos }}</td>
                            <td>{{ $fichajuadors->talla_camisa_polo }}</td>
                            <td>{{ $fichajuadors->talla_pantalon }}</td>
                            <td>{{ $fichajuadors->licencia }}</td>
                            <td>{{ $fichajuadors->fecha_nacimiento }}</td>

                            <td><a href="{{ route('fichasjugador.edit', $fichajuadors)}}" class="btn btn-primary"><i class="fas fa-file-pdf"></i></i></a></td>
                            <td>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{'#exampleModalCenter'.$fichajuadors->id}}">
                                    <i class="fas fa-trash"></i>
                                  </button>

                                    <!-- Modal -->
                                        <div class="modal fade" id="{{'exampleModalCenter'.$fichajuadors->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">Confirmar Borrado</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        </div>
                                                        <div class="modal-body">
                                                        ¿Está seguro de que quiere borrar la noticia "{{ $fichajuadors->nombre }}"?
                                                        </div>
                                                        <div class="modal-footer">
                                                        <button type="button" class="btn btn-success" data-dismiss="modal">Cancelar</button>

                                                        <form action="{{ route('fichasjugador.destroy', $fichajuadors)}}" method="POST">
                                                            @csrf
                                                            @method('delete')
                                                            <button type="submit" class="btn btn-danger">Aceptar</button>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <!-- Fin Modal -->
                            </td>
                            
                        </tr>
                        
                    @endforeach
                </tbody>
            </table>
            {{ $fichajuador->links() }}
        </div>
    </div>

@stop