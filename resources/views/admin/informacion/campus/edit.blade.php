@extends('adminlte::page')

@section('title')

@section('content_header')
    <h1>Editar Información del Campus</h1>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        {!! Form::model($infocampus,['route' => ['infocampus.update', $infocampus], 'files' => true, 'method' => 'put']) !!}
            @csrf
            <div class="form-group">
                {!! Form::label('titulo', 'Titulo:') !!}
                {!! Form::text('titulo', null, [ 'class' => 'form-control', 'placeholder' => 'Introduzca el Titulo']) !!}

                @error('titulo')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group">
                {!! Form::label('texto', 'Información:') !!}
                {!! Form::textarea('texto', null, [ 'class' => 'form-control', 'placeholder' => 'Redacte la Información']) !!}

                @error('texto')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            {!! Form::submit('Actualizar Información', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
    </div>
</div>
@stop

@section('js')
<script src="https://cdn.ckeditor.com/ckeditor5/27.1.0/classic/ckeditor.js"></script>

<script>
    ClassicEditor
    .create( document.querySelector( '#texto' ) )
    .catch( error => {
        console.error( error );
    });

</script>
@stop