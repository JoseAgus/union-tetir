@extends('adminlte::page')

@section('title')

@section('content_header')
    <h1>Información de la Reserva</h1>
@stop

@section('content')

@if (session('info'))
    <div class="alert alert-info">
        <strong>{{ session('info')}}</strong>
    </div>
@endif
    
    <div class="card">
        <div class="card-body">
            @foreach ($inforeserva as $inforeservas)

                <a href="{{ route('inforeserva.edit', $inforeservas)}}" class="btn btn-primary btn-lg float-right"><i class="fas fa-edit"></i></a>
                <h1>Titulo</h1>
                <br>
                <h2> {{ $inforeservas->titulo}} </h2>
                <br>
                <h1>Informacion:</h1>

                {!! $inforeservas->texto!!}
                
            @endforeach
            <br>
        </div>
    </div>

@stop