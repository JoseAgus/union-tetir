@extends('adminlte::page')

@section('title')

@section('content_header')
    <h1>Competiciones</h1>
@stop

@section('content')

@if (session('info'))
    <div class="alert alert-info">
        <strong>{{ session('info')}}</strong>
    </div>
@endif

@if (session('create'))
    <div class="alert alert-success">
        <strong>{{ session('create')}}</strong>
    </div>
@endif

@if (session('destroy'))
    <div class="alert alert-danger">
        <strong>{{ session('destroy')}}</strong>
    </div>
@endif
    
    <div class="card">
        <div class="card-body">

            <a href="{{ route('competicion.create')}}" class="btn btn-success float-right">Nueva Competicion</a>
            <form action="{{ route('competicion.index')}}" method="GET">
                @csrf
                <div class="form-row">
                    <div class="col-auto">
                        <input type="submit" class="btn btn-info" value="Buscar">
                    </div>
                
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="texto" placeholder="Busque por Nombre de la Competicion" value="{{$texto}}">
                    </div>
                </div>
            </form>
            <br>
            <br>
            @foreach ($categoria as $categorias)
    
                <div class="card col-12 py-3">
                    <div class="card-header border-0">

                        <h3>{{$categorias->nombre}}</h3>
                        <h3>{{$categorias->id}}</h3>
                        <div class="float-left">
                            @if ( $categorias->estado == 1)
                                <h4 style="background-color: red; border-radius: 5%; color: #fff">No Publicado</h4>
                            @else
                                <h4 style="background-color: green; border-radius: 5%;  color: #fff">Publicado</h4>
                            @endif
                        </div>
                        <div class="card-tools">

                            <a href="{{ route('competicion.edit', $categorias)}}" class="btn btn-primary">
                            <i class="fas fa-edit"></i>
                            </a>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{'#exampleModalCenter'.$categorias->id}}">
                                <i class="fas fa-trash"></i>
                              </button>

                                <!-- Modal -->
                                    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="{{'exampleModalCenter'.$categorias->id}}">Confirmar Borrado</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    </div>
                                                    <div class="modal-body">
                                                    ¿Está seguro de que quiere borrar la noticia "{{ $categorias->nombre }}"?
                                                    </div>
                                                    <div class="modal-footer">
                                                    <button type="button" class="btn btn-success" data-dismiss="modal">Cancelar</button>

                                                    <form action="{{ route('competicion.destroy', $categorias)}}" method="POST">
                                                        @csrf
                                                        @method('delete')
                                                        <button type="submit" class="btn btn-danger">Aceptar</button>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <!-- Fin Modal -->
                        </div>
                    </div>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-striped table-valign-middle">
                            <thead>
                                <tr>
                                    <th>Nombre del Club</th>
                                    <th>Puntos</th>
                                    <th>Partidos Ganados</th>
                                    <th>Partidos Empatados</th>
                                    <th>Partidos Perdidos</th>
                                    <th>Goles a Favor</th>
                                    <th>Goles en Contra</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($categorias->clasificacion()->orderBy('puntos_totales', 'desc')->get() as $clasificacione)
                                    <tr>
                                        <td>{{ $clasificacione->nombre_club }}</td>
                                        <td>{{ $clasificacione->puntos_totales }}</td>
                                        <td>{{ $clasificacione->partidos_ganados }}</td>
                                        <td>{{ $clasificacione->partidos_empatados }}</td>
                                        <td>{{ $clasificacione->partidos_perdidos }}</td>
                                        <td>{{ $clasificacione->goles_a_favor }}</td>
                                        <td>{{ $clasificacione->goles_en_contra }}</td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            @endforeach

            
            
        </div>
        {{ $categoria->links() }}
    </div>

@stop