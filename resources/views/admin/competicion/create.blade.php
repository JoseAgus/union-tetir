@extends('adminlte::page')

@section('title')

@section('content_header')
    <h1>Crear Competición</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            {!! Form::open(['route' => 'competicion.store']) !!}
                @csrf
                {!! Form::hidden('user_id', auth()->user()->id) !!}


                <div class="form-group">
                    <p class="font-weight-bold">Estado:</p>
     
                    <label class="mr-3">
                        {!! Form::radio('estado', 1, true) !!}
                        No Publicar
                    </label>
                    <label class="mr-3">
                        {!! Form::radio('estado', 2) !!}
                        Publicar
                    </label>
     
                    @error('estado')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
     
                </div>

                <div class="form-group">
                    {!! Form::label('nombre', 'Nombre de la Competición:') !!}
                    {!! Form::text('nombre', null, [ 'class' => 'form-control', 'placeholder' => 'Introduzca un Nombre de la Competición']) !!}

                    @error('nombre')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

                <div class="form-group">
                    {!! Form::label('nombre_breve', 'Nombre Corto:') !!}
                    {!! Form::text('nombre_breve', null, [ 'class' => 'form-control', 'placeholder' => 'Introduzca un Nombre Breve para la Competición']) !!}

                    @error('nombre_breve')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <br>
                <table id="tabla" class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nombre del Club</th>
                            <th>Puntos</th>
                            <th>Partidos Ganados</th>
                            <th>Partidos Empatados</th>
                            <th>Partidos Perdidos</th>
                            <th>Goles a Favor</th>
                            <th>Goles en Contra</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                         {{-- Linea Para Clonar --}}
                            <tr class="fila-fija" id="Clon">
                                <td>
                                    <div class="form-group">
                                        {!! Form::text('club[]', null, ['class' => 'form-control']) !!}
                                    </div>
                                </td>
                                <td>
                                    <div class="quantity">
                                        {!! Form::number('pt[]', 0, ['setp' => '1', 'min' => '-999', 'max' => '999']) !!}
                                    </div>
                                </td>

                                <td>
                                    <div class="quantity">
                                        {!! Form::number('pg[]', 0, ['setp' => '1', 'min' => '-999', 'max' => '999']) !!}
                                    </div>
                                </td>

                                <td>
                                    <div class="quantity">
                                        {!! Form::number('pe[]', 0, ['setp' => '1', 'min' => '-999', 'max' => '999']) !!}
                                    </div>
                                </td>

                                <td>
                                    <div class="quantity">
                                        {!! Form::number('pp[]', 0, ['setp' => '1', 'min' => '-999', 'max' => '999']) !!}
                                    </div>
                                </td>

                                <td>
                                    <div class="quantity">
                                        {!! Form::number('gf[]', 0, ['setp' => '1', 'min' => '-999', 'max' => '999']) !!}
                                    </div>
                                </td>

                                <td>
                                    <div class="quantity">
                                        {!! Form::number('gc[]', 0, ['setp' => '1', 'min' => '-999', 'max' => '999']) !!}
                                    </div>
                                </td>

                                <td class='eliminar'>
                                    <input class="btn btn-danger" type="button" value="Eliminar -">
                                </td>
                            </tr>
                        {{-- Primera Fila --}}
                            <tr class="">
                                <td>
                                    <div class="form-group">
                                        {!! Form::text('club[]', null, ['class' => 'form-control']) !!}
                                    </div>
                                </td>
                                <td>
                                    <div class="quantity">
                                        {!! Form::number('pt[]', 0, ['setp' => '1', 'min' => '-999', 'max' => '999']) !!}
                                    </div>
                                </td>

                                <td>
                                    <div class="quantity">
                                        {!! Form::number('pg[]', 0, ['setp' => '1', 'min' => '-999', 'max' => '999']) !!}
                                    </div>
                                </td>

                                <td>
                                    <div class="quantity">
                                        {!! Form::number('pe[]', 0, ['setp' => '1', 'min' => '-999', 'max' => '999']) !!}
                                    </div>
                                </td>

                                <td>
                                    <div class="quantity">
                                        {!! Form::number('pp[]', 0, ['setp' => '1', 'min' => '-999', 'max' => '999']) !!}
                                    </div>
                                </td>

                                <td>
                                    <div class="quantity">
                                        {!! Form::number('gf[]', 0, ['setp' => '1', 'min' => '-999', 'max' => '999']) !!}
                                    </div>
                                </td>

                                <td>
                                    <div class="quantity">
                                        {!! Form::number('gc[]', 0, ['setp' => '1', 'min' => '-999', 'max' => '999']) !!}
                                    </div>
                                </td>

                                <td class='eliminar'>
                                    <input class="btn btn-danger" type="button" value="Eliminar -">
                                </td>
                            </tr>
                    </tbody>
                </table>
                <div class="btn-der float-right">

					<button id="adicional" name="adicional" type="button" class="btn btn-warning"> Añadir Clasificación +  </button>

				</div>
               
                {!! Form::submit('Crear Competicion', ['class' => 'btn btn-primary', 'id' => 'deleteClon']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop

@section('css')
    <style>
        .quantity {
        position: relative;
        }

        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button
        {
        -webkit-appearance: none;
        margin: 0;
        }

        input[type=number]
        {
        -moz-appearance: textfield;
        }

        .quantity input {
        width: 70px;
        height: 42px;
        line-height: 1.65;
        float: left;
        display: block;
        padding: 0;
        margin: 0;
        padding-left: 20px;
        border: 1px solid #eee;
        }

        .quantity input:focus {
        outline: 0;
        }

        .quantity-nav {
        float: left;
        position: relative;
        height: 42px;
        }

        .quantity-button {
        position: relative;
        cursor: pointer;
        border-left: 1px solid #eee;
        width: 20px;
        text-align: center;
        color: #333;
        font-size: 13px;
        font-family: "Trebuchet MS", Helvetica, sans-serif !important;
        line-height: 1.7;
        -webkit-transform: translateX(-100%);
        transform: translateX(-100%);
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        -o-user-select: none;
        user-select: none;
        }

        .quantity-button.quantity-up {
        position: absolute;
        height: 50%;
        top: 0;
        border-bottom: 1px solid #eee;
        }

        .quantity-button.quantity-down {
        position: absolute;
        bottom: -1px;
        height: 50%;
        }

        .fila-fija{
        display: none;
        }
    </style>
@stop

@section('js')
    <script src="{{ asset('js/numero.js') }}"></script>
    <script>
			
        $(function(){
            // Clona la fila oculta que tiene los campos base, y la agrega al final de la tabla
            $("#adicional").on('click', function(){
                $("#tabla tbody tr:eq(0)").clone().removeClass('fila-fija').appendTo("#tabla");
            });
         
            //Borra la tabla Clon para que no interfiera con la BD
            $("#deleteClon").on('click', function(){
                $("#Clon").remove();
            });

            // Evento que selecciona la fila y la elimina 
            $(document).on("click",".eliminar",function(){
                var parent = $(this).parents().get(0);
                $(parent).remove();
            });
        });
    </script>
@stop