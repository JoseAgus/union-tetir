@extends('adminlte::page')

@section('title')

@section('content_header')
    <h1>Noticias</h1>
@stop

@section('content')

@if (session('info'))
    <div class="alert alert-info">
        <strong>{{ session('info')}}</strong>
    </div>
@endif

@if (session('create'))
    <div class="alert alert-success">
        <strong>{{ session('create')}}</strong>
    </div>
@endif

@if (session('destroy'))
    <div class="alert alert-danger">
        <strong>{{ session('destroy')}}</strong>
    </div>
@endif
    
    <div class="card">
        <div class="card-body">
        <a href="{{ route('noticias.create')}}" class="btn btn-success float-right">Nueva Noticia</a>

            <form action="{{ route('noticias.index')}}" method="GET">
                @csrf
                <div class="form-row">
                    <div class="col-auto">
                        <input type="submit" class="btn btn-info" value="Buscar">
                    </div>
                
                    <div class="col-sm-4">
                        <input type="text" class="form-control" name="texto" placeholder="Busque por Titulo o por Descripcion" value="{{$texto}}">
                    </div>
                </div>
            </form>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Titulo</th>
                        <th>Descripcion</th>
                        <th>Fecha</th>
                        <th colspan="2"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($noticia as $noticias)
                        <tr>
                            <td>{{ $noticias->id }}</td>
                            <td>{{ $noticias->titulo }}</td>
                            <td>{{ $noticias->descripcion }}</td>
                            <td>{{ $noticias->created_at }}</td>
                            <td class="text-center">
                                @if ( $noticias->estado == 1)
                                    <p style="background-color: red; border-radius: 5%; color: #fff">No Publicado</p>
                                @else
                                    <p style="background-color: green; border-radius: 5%;  color: #fff">Publicado</p>
                                @endif
                            </td>
                            <td><a href="{{ route('noticias.edit',$noticias)}}" class="btn btn-primary"><i class="fas fa-edit"></i></a></td>
                            <td>
                                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{'#exampleModalCenter'.$noticias->id}}">
                                    <i class="fas fa-trash"></i>
                                  </button>

                                    <!-- Modal -->
                                        <div class="modal fade" id="{{'exampleModalCenter'.$noticias->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">Confirmar Borrado</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        </div>
                                                        <div class="modal-body">
                                                        ¿Está seguro de que quiere borrar la noticia "{{ $noticias->titulo }}"?
                                                        </div>
                                                        <div class="modal-footer">
                                                        <button type="button" class="btn btn-success" data-dismiss="modal">Cancelar</button>

                                                        <form action="{{ route('noticias.destroy', $noticias)}}" method="POST">
                                                            @csrf
                                                            @method('delete')
                                                            <button type="submit" class="btn btn-danger">Aceptar</button>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <!-- Fin Modal -->
                            </td>
                            
                        </tr>
                        
                    @endforeach
                </tbody>
            </table>
            {{ $noticia->links() }}
        </div>
    </div>

@stop