@extends('adminlte::page')

@section('title')

@section('content_header')
    <h1>Editar Noticia</h1>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        {!! Form::model($carrusel,['route' => ['carrusel.update', $carrusel], 'files' => true, 'method' => 'put']) !!}
            @csrf
            {!! Form::hidden('user_id', auth()->user()->id) !!}

            <div class="form-group">
                {!! Form::label('nombre', 'Nombre:') !!}
                {!! Form::text('nombre', null, [ 'class' => 'form-control', 'placeholder' => 'Introduzca el Nombre de la Imagen']) !!}

                @error('nombre')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group">
                <p class="font-weight-bold">Estado:</p>
 
                <label class="mr-3">
                    {!! Form::radio('estado', 1, true) !!}
                    No Publicar
                </label>
                <label class="mr-3">
                    {!! Form::radio('estado', 2) !!}
                    Publicar
                </label>
 
                @error('estado')
                    <span class="text-danger" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
 
            </div>

            <div class="row mr-3">
                <div class="col">
                    <div class="image-warp">
                        @if ($carrusel->image)
                        <img id="picture" src="{{ asset('storage/'.$carrusel->image->url) }}" alt="Imagen">
                    @else
                        <img id="picture" src="https://cdn.pixabay.com/photo/2021/02/21/18/48/elks-6037526_1280.jpg" alt="Imagen">
                    @endif
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        {!! Form::label('file', 'Imagen Portada') !!}
                        {!! Form::file('file', ['class' => 'form-control', 'accept' => 'image/*']) !!}
                        <h4>Requisistos de la imagen</h4>
                        <p>Para que la imagen se vea bien el la pagina debeta tener:</p>
                        <p>- El tamaño de la imangen debera ser de 1600 x 545, podria ser mas grande pero siguiendo la misma
                            estetica</p>
                        <p>- El peso de la imagen no puede ser superior a 5Mb</p>
                        <p>- Escoja una imagen adecuada</p>
 
                        @error('file')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
 
            </div>
            <br>
            {!! Form::submit('Actualizar Imagen', ['class' => 'btn btn-primary']) !!}
        {!! Form::close() !!}
    </div>
</div>
@stop

@section('css')
<style>
.image-warp {
    position: relative;
    padding-bottom: 56.25%
}

.image-warp img {
    position: absolute;
    object-fit: cover;
    width: 100%;
    height: 100%;
}

</style>
@stop

@section('js')
<script src="https://cdn.ckeditor.com/ckeditor5/27.1.0/classic/ckeditor.js"></script>

<script>

ClassicEditor
.create( document.querySelector( '#texto' ) )
.catch( error => {
    console.error( error );
});

//Cambiar imagen
document.getElementById("file").addEventListener('change', cambiarImagen);

function cambiarImagen(event) {
    var file = event.target.files[0];

    var reader = new FileReader();
    reader.onload = (event) => {
        document.getElementById("picture").setAttribute('src', event.target.result);
    };

    reader.readAsDataURL(file);
}

</script>
@stop