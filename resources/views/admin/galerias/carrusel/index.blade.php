@extends('adminlte::page')

@section('title')

@section('content_header')
    <h1>Imagenes de Portada</h1>
@stop

@section('content')
@if (session('info'))
<div class="alert alert-info">
    <strong>{{ session('info')}}</strong>
</div>
@endif

@if (session('create'))
<div class="alert alert-success">
    <strong>{{ session('create')}}</strong>
</div>
@endif

@if (session('destroy'))
<div class="alert alert-danger">
    <strong>{{ session('destroy')}}</strong>
</div>
@endif

<div class="card">
    <div class="card-body">
    <a href="{{ route('carrusel.create')}}" class="btn btn-success float-right">Nueva Imagen</a>

        <form action="{{ route('carrusel.index')}}" method="GET">
            @csrf
            <div class="form-row">
                <div class="col-auto">
                    <input type="submit" class="btn btn-info" value="Buscar">
                </div>
            
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="texto" placeholder="Busque por el Nombre" value="{{$texto}}">
                </div>
            </div>
        </form>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th colspan="3"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($carrusel as $carrusels)
                    <tr>
                        <td>{{ $carrusels->id }}</td>
                        <td>{{ $carrusels->nombre }}</td>
                        <td class="text-center">
                            @if ( $carrusels->estado == 1)
                                <p class="w-50" style="background-color: red; border-radius: 5%; color: #fff">No Publicado</p>
                            @else
                                <p class="w-50" style="background-color: green; border-radius: 5%;  color: #fff">Publicado</p>
                            @endif
                        </td>
                        <td><a href="{{ route('carrusel.edit',$carrusels)}}" class="btn btn-primary"><i class="fas fa-edit"></i></a></td>
                        <td>
                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="{{'#exampleModalCenter'.$carrusels->id}}">
                                <i class="fas fa-trash"></i>
                              </button>

                                <!-- Modal -->
                                    <div class="modal fade" id="{{'exampleModalCenter'.$carrusels->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLongTitle">Confirmar Borrado</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    </div>
                                                    <div class="modal-body">
                                                    ¿Está seguro de que quiere borrar la imagen "{{ $carrusels->nombre }}"?
                                                    </div>
                                                    <div class="modal-footer">
                                                    <button type="button" class="btn btn-success" data-dismiss="modal">Cancelar</button>

                                                    <form action="{{ route('carrusel.destroy', $carrusels)}}" method="POST">
                                                        @csrf
                                                        @method('delete')
                                                        <button type="submit" class="btn btn-danger">Aceptar</button>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <!-- Fin Modal -->
                        </td>
                        
                    </tr>
                    
                @endforeach
            </tbody>
        </table>
        {{ $carrusel->links() }}
    </div>
</div>
@stop