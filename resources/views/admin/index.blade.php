@extends('adminlte::page')

@section('title')

@section('content_header')
    <h1>Panel de Administración</h1>
@stop

@section('content')
    <div class="row text-center">
        @can('solo.admin')
            <div class="col-lg-12 col-12">
                <!-- small box -->
                <div class="small-box bg-info">
                <div class="inner">
                    <h3>Usuarios</h3>

                    <p>Ver una lista de todos los usuarios</p>
                </div>
                <div class="icon">
                    <i class="fas fa-user"></i>
                </div>
                <a href="/admin/users" class="small-box-footer">Mas Información <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        @endcan
        @can('solo.admin.noticia')
            <div class="col-lg-12 col-12">
                <!-- small box -->
                <div class="small-box bg-success">
                <div class="inner">
                    <h3>Noticias</h3>

                    <p>Ver una lista de todas las noticias</p>
                </div>
                <div class="icon">
                    <i class="fas fa-newspaper"></i>
                </div>
                <a href="/admin/noticias" class="small-box-footer">Mas Información <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        @endcan
        @can('solo.admin')

            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                <div class="inner">
                    <h3>Reservas</h3>

                    <p>Ver una lista de todas las reservas</p>
                </div>
                <div class="icon">
                    <i class="fas fa-file-signature"></i>
                </div>
                <a href="/admin/reservas" class="small-box-footer">Mas Información <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                <div class="inner">
                    <h3>Ficha Jugador</h3>

                    <p>Ver una lista de todas las fichas de jugador</p>
                </div>
                <div class="icon">
                    <i class="fas fa-file-signature"></i>
                </div>
                <a href="/admin/fichasjugador" class="small-box-footer">Mas Información <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-warning">
                <div class="inner">
                    <h3>Inscripciones al Campus</h3>

                    <p>Ver una lista de todas inscripciones al campus</p>
                </div>
                <div class="icon">
                    <i class="fas fa-file-signature"></i>
                </div>
                <a href="/admin/inscripcionescampus" class="small-box-footer">Mas Información <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-secondary">
                <div class="inner">
                    <h3>Torneo Navidad</h3>

                    <p>Ver la información del torneo navidad</p>
                </div>
                <div class="icon">
                    <i class="fas fa-info-circle"></i>
                </div>
                <a href="/admin/infonavidad" class="small-box-footer">Mas Información <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-secondary">
                <div class="inner">
                    <h3>Campus</h3>

                    <p>Ver la información del campus</p>
                </div>
                <div class="icon">
                    <i class="fas fa-info-circle"></i>
                </div>
                <a href="/admin/infocampus" class="small-box-footer">Mas Información <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-secondary">
                <div class="inner">
                    <h3>Reserva</h3>

                    <p>Ver la información del reserva</p>
                </div>
                <div class="icon">
                    <i class="fas fa-info-circle"></i>
                </div>
                <a href="/admin/inforeserva" class="small-box-footer">Mas Información <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-primary">
                <div class="inner">
                    <h3>Banners</h3>

                    <p>Ver una lista de las imagenes del banner</p>
                </div>
                <div class="icon">
                    <i class="fas fa-images"></i>
                </div>
                <a href="/admin/carrusel" class="small-box-footer">Mas Información <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-primary">
                <div class="inner">
                    <h3>Patrocinadores</h3>

                    <p>Ver una lista de las imagenes de los patrocinadores y colaboradores</p>
                </div>
                <div class="icon">
                    <i class="fas fa-images"></i>
                </div>
                <a href="/admin/patrocinadores" class="small-box-footer">Mas Información <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-4 col-6">
                <!-- small box -->
                <div class="small-box bg-primary">
                <div class="inner">
                    <h3>Torneo Navidad</h3>

                    <p>Ver una lista de las imagenes de la galeria del torneo de navidad</p>
                </div>
                <div class="icon">
                    <i class="fas fa-images"></i>
                </div>
                <a href="/admin/galerianavidad" class="small-box-footer">Mas Información <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col-lg-12 col-12">
                <!-- small box -->
                <div class="small-box bg-danger">
                <div class="inner">
                    <h3>Competiciones</h3>

                    <p>Ver una lista de las Competiciones</p>
                </div>
                <div class="icon">
                    <i class="fas fa-futbol"></i>
                </div>
                <a href="/admin/competicion" class="small-box-footer">Mas Información <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        @endcan


    </div>

@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
    <script> console.log('Hi!'); </script>
@stop