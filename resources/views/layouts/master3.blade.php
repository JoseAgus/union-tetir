<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>www.uniontetir.com – Club de Fútbol Unión Tetir</title>

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">

    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('plugins/owl.carousel2/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/owl.carousel2/assets/owl.theme.default.min.css') }}">

    <!-- Google fonts-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">

    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.default.css') }}" id="theme-stylesheet">

    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/jquery/slick/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/jquery/slick/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('img/Escudo-Union-Tetir.png') }}">

</head>

<body>
    @include('partials.navbar3')

        @yield('content')

</body>