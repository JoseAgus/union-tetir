<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>www.uniontetir.com – Club de Fútbol Unión Tetir</title>

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">

    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="{{ asset('plugins/owl.carousel2/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/owl.carousel2/assets/owl.theme.default.min.css') }}">

    <!-- Google fonts-->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">

    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.default.css') }}" id="theme-stylesheet">

    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/jquery/slick/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/jquery/slick/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

    <!-- Favicon-->
    <link rel="shortcut icon" href="{{ asset('img/Escudo-Union-Tetir.png') }}">

    <style>
        :root {
            --color-bg-1: #008080;
            --color-bg-2: #FEF9E7;
            --color-btn: #2196F3;
            --color-header-step: orange;
            --color-border-input: #ddd;
        }
        /* Progressbar */
        .progressbar {
            display: flex;
            list-style: none;
            margin-bottom: 15px;
            counter-reset: step;
        }

        .progressbar__option {
            width: 100%;
            text-align: center;
            font-size: .7rem;
            text-transform: uppercase;
            position: relative;
        }

        .progressbar__option:before {
            display: flex;
            content: counter(step);
            counter-increment: step;
            width: 20px;
            height: 20px;
            background-color: white;
            margin: 0 auto 5px;
            border-radius: 3px;
            justify-content: center;
            align-items: center;
            position: relative;
            z-index: 2;
        }


        .progressbar__option:after {
            display: block;
            content: '';
            width: 100%;
            height: 2px;
            background-color: white;
            position: absolute;
            top: 10px;
            left: -50%;
            z-index: 1;
        }

        .progressbar__option:first-child:after {
            content: none;
        }

        .progressbar__option.active:before, .progressbar__option.active:after {
            background-color: var(--color-header-step);
        }

        /* Título del formulario */
        .form-register__title {
            font-size: 1.4rem;
            text-align: center;
            margin-bottom: 15px;
        }

        /* step */
        .step {
            background-color: white;
            border-radius: 3px;
            min-width: 100%;
            opacity: 0;
            transition: all .2s linear;
        }

        .step.active {
            display: block;
            opacity: 1;
        }

        .step.to-left {
            display: none;
            margin-left: -100%;
        }

        .step.inactive {
            animation-name: scale;
            animation-duration: .2s;
            animation-direction: alternate;
            animation-iteration-count: 2;
        }

        @keyframes scale {
            from {
                transform: scale(1);
            }
            to {
                transform: scale(1.1);
            }
        }

        /* header de step */
        .step__header {
            padding: 20px 15px;
            background-color: var(--color-header-step);
            border-radius: 3px 3px 0 0;
        }

        .step__title {
            font-size: 1.1rem;
            text-align: center;
        }

        /* body de step */
        .step__body {
            padding: 20px 15px 0;
        }

        /* step inputs */
        .step__input {
            display: block;
            width: 100%;
            padding: 10px;
            margin-bottom: 10px;
            border-radius: 3px;
            border: 1px solid var(--color-border-input);
        }

        /* step footer */
        .step__footer {
            padding: 20px 15px;
            text-align: center;
        }

        /* step botones */
        .step__button {
            display: inline-block;
            padding: 10px;
            background-color: var(--color-btn);
            border: none;
            color: white;
            border-radius: 3px;
            cursor: pointer;
        }
    </style>

</head>

<body>
    @include('partials.navbar2')

        @yield('content')

    @include('partials.footer')
</body>