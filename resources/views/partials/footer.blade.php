<footer class="bg-dark py-4">
    <div class="container">
        <div class="row py-2">
            <div class="col-lg-4 text-center text-lg-left mb-2 mb-lg-0">
                <p class="small text-uppercase mb-0">
                    <a class="reset-anchor" href="#">&copy; 2019 Unión Tetir CF</a>
                    <a class="reset-anchor">|</a>
                    <a class="reset-anchor" href="#">Política de Privacidad</a>
                </p>
            </div>
            <div class="col-lg-4 text-center mb-2 mb-lg-0">
                <ul class="list-inline mb-0">
                    <li class="list-inline-item"><a class="reset-anchor" href="https://es-es.facebook.com/pages/category/Sports-Team/Unin-Tetir-Club-de-Ftbol-205491079623284/"><i
                                class="fab fa-facebook-f"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-4 text-center text-lg-right">
                <p class="small text-uppercase mb-0"><a class="reset-anchor" href="mailto:uniontetircf@hotmail.com"><i class="fa fa-envelope"></i>
                        uniontetircf@hotmail.com</a></p>
            </div>
        </div>
    </div>
</footer>


<!-- Links -->
<script type="text/javascript" src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugins/jquery/slick/slick.min.js') }}"></script>
<script src="{{ asset('plugins/owl.carousel2/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/front.js') }}"></script>
<script src="{{ asset('js/pasos.js') }}"></script>
<script src="{{ asset('js/numero.js') }}"></script>

<script type="text/javascript">
    $(function() {
        $('.autoplay').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: false,
            autoplay: true,
            autoplaySpeed: 2000,
        });
    });
</script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
