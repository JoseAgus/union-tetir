    <!-- Header-->
    <header class="header">
        <!-- Top bar -->
        <div class="py-2 bg-dark">
            <div class="container py-1">
                <div class="row align-items-center">
                    @auth
                        <div class="col-lg-6">
                            <ul class="list-inline mb-0 d-md-center d-sm-center d-md-center-min">
                                <li class="list-inline-item"><a class="reset-anchor"><i class="fa fa-phone"></i> 629 413
                                        476</a></li>
                                <li class="list-inline-item">|</li>
                                <li class="list-inline-item"><a class="reset-anchor"> 636 476 777</a></li>
                                <li class="list-inline-item"></li>
                                <li class="list-inline-item"><a class="reset-anchor" href="mailto:uniontetircf@hotmail.com"><i class="fa fa-envelope"></i>
                                        uniontetircf@hotmail.com</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-6 col-md-12 text-lg-right">
                            <div class="dropdown mb-0 d-md-center d-sm-center ">
                                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ Auth::user()->name }}
                                </button>
        
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        
                                    @can('solo.admin.noticia')
                                        <a class="dropdown-item" href="/admin">Admin</a>
                                    @endcan
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="gg-log-out"></i>Cerrar Sesión
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </div>
    
                    @else
                        <div class="col-lg-6">
                            <ul class="list-inline mb-0 d-md-center d-sm-center d-md-center-min">
                                <li class="list-inline-item"><a class="reset-anchor"><i class="fa fa-phone"></i> 629 413
                                        476</a></li>
                                <li class="list-inline-item">|</li>
                                <li class="list-inline-item"><a class="reset-anchor"> 636 476 777</a></li>
                                <li class="list-inline-item"></li>
                                <li class="list-inline-item"><a class="reset-anchor" href="mailto:uniontetircf@hotmail.com"><i class="fa fa-envelope"></i>
                                        uniontetircf@hotmail.com</a></li>
                            </ul>
                        </div>
                        <!-- Alternativa 1 -->
                        <!-- <div class="col-lg-4 d-none d-lg-block text-right">
                            <ul class="list-inline mb-0">

                                <li class="list-inline-item"><a class="reset-anchor" href="https://es-es.facebook.com/pages/category/Sports-Team/Unin-Tetir-Club-de-Ftbol-205491079623284/"><i class="fab fa-facebook-f"></i></a></li>

                            </ul>
                        </div> -->

                        <!-- Alternativa 2 -->
                        <div class="col-lg-5 col-md-12 text-lg-right">
                            <ul class="list-inline mb-0 d-md-center d-sm-center">

                                <li class="list-inline-item"><a class="reset-anchor" href="{{ route('login') }}">Iniciar sesión</a></li>
                                <li class="list-inline-item">|</li>
                                <li class="list-inline-item"><a class="reset-anchor" href="{{ route('register') }}">Registrarse</a></li>

                            </ul>
                        </div>
                    @endauth
                </div>
            </div>
        </div>
        <!-- Navbar 1 -->
        <nav class="navbar navbar-expand-lg navbar-light bg-white">
            <div class="container text-center">
                <a class="navbar-brand mx-auto" href="/"><img class="mb-2" src="img/Escudo-Union-Tetir.png" alt="Asociación del Union Tetir C.F." width="140">
                    <p class="text-uppercase mb-0" style="color: black;"><b>UNIÓN TETIR C.T.</b></p>
                </a>
            </div>
        </nav>
        <!-- Navbar 2 -->
        <nav class="navbar navbar-expand-lg navbar-light border-gray py-2 bg-light">
            <div class="container">
                <button class="navbar-toggler navbar-toggler-right mx-auto border-0" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span
                        class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse text-center" id="navbarSupportedContent">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item px-1">
                            <!-- Link--><a class="nav-link" href="/">Inicio</a>
                        </li>
                        <li class="nav-item px-1 dropdown"><a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">El Club</a>
                            <div class="dropdown-menu text-center text-lg-left" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="/historia">Historia</a>
                                <a class="dropdown-item" href="/biografia">Domingo Juan Vera Alonso</a>
                        </li>
                        <li class="nav-item px-1 dropdown"><a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Inscripción</a>
                            <div class="dropdown-menu text-center text-lg-left" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="/reserva">Reservar plaza</a>
                                <a class="dropdown-item" href="/ficha-jugador">Ficha Jugador</a>
                        </li>
                        <li class="nav-item px-1">
                            <!-- Link--><a class="nav-link" href="#Clasificasiones">Clasificacion</a>
                        </li>
                        <li class="nav-item px-1 dropdown"><a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Eventos</a>
                            <div class="dropdown-menu text-center text-lg-left" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="/campus">Campus</a>
                                <a class="dropdown-item" href="/torneo-navidad">Torneo Navideño</a>
                        </li>
                        <li class="nav-item px-1">
                            <!-- Link--><a class="nav-link" href="/noticias">Noticias</a>
                        </li>
                        <li class="nav-item px-1">
                            <!-- Link--><a class="nav-link" href="/contactanos">Contáctenos</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>