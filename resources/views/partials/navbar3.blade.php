    <!-- Header-->
    <header class="header">
        <!-- Top bar -->
        <div class="py-2 bg-dark">
            <div class="container py-1">
                <div class="row align-items-center">
                    @auth
                        <div class="col-lg-6">
                            <ul class="list-inline mb-0 d-md-center d-sm-center d-md-center-min">
                                <li class="list-inline-item"><a class="reset-anchor"><i class="fa fa-phone"></i> 629 413
                                        476</a></li>
                                <li class="list-inline-item">|</li>
                                <li class="list-inline-item"><a class="reset-anchor"> 636 476 777</a></li>
                                <li class="list-inline-item"></li>
                                <li class="list-inline-item"><a class="reset-anchor" href="mailto:uniontetircf@hotmail.com"><i class="fa fa-envelope"></i>
                                        uniontetircf@hotmail.com</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-6 col-md-12 text-lg-right">
                            <div class="dropdown mb-0 d-md-center d-sm-center ">
                                <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ Auth::user()->name }}
                                </button>
        
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        
                                    @can('solo.admin.noticia')
                                        <a class="dropdown-item" href="/admin">Admin</a>
                                    @endcan
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        <i class="gg-log-out"></i>Cerrar Sesión
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </div>
                        </div>
    
                    @else
                        <div class="col-lg-6">
                            <ul class="list-inline mb-0 d-md-center d-sm-center d-md-center-min">
                                <li class="list-inline-item"><a class="reset-anchor"><i class="fa fa-phone"></i> 629 413
                                        476</a></li>
                                <li class="list-inline-item">|</li>
                                <li class="list-inline-item"><a class="reset-anchor"> 636 476 777</a></li>
                                <li class="list-inline-item"></li>
                                <li class="list-inline-item"><a class="reset-anchor" href="mailto:uniontetircf@hotmail.com"><i class="fa fa-envelope"></i>
                                        uniontetircf@hotmail.com</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-5 col-md-12 text-lg-right">
                            <ul class="list-inline mb-0 d-md-center d-sm-center">

                                <li class="list-inline-item"><a class="reset-anchor" href="{{ route('login') }}">Iniciar sesión</a></li>
                                <li class="list-inline-item">|</li>
                                <li class="list-inline-item"><a class="reset-anchor" href="{{ route('register') }}">Registrarse</a></li>

                            </ul>
                        </div>
                    @endauth
                </div>
            </div>
        </div>
        <!-- Navbar 1 -->
        <nav class="navbar navbar-expand-lg navbar-light bg-white">
            <div class="container text-center">
                <a class="navbar-brand mx-auto" href="/"><img class="mb-2" src="{{ asset('img/Escudo-Union-Tetir.png') }}" alt="Asociación del Union Tetir C.F." width="140">
                    <p class="text-uppercase mb-0" style="color: black;"><b>UNIÓN TETIR C.T.</b></p>
                </a>
            </div>
        </nav>
    </header>