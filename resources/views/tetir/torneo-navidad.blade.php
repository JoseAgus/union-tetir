@extends('layouts.master2')

@section('content')
<section>
    <div class="container">
        <div class="row">
            <div id="contenedorH" class="col-lg-12 mb-5 mb-lg-0">
                {{-- Poner el titulo  --}}
                @foreach ($torneoNavidad as $torneoNavida)
                    <h1>{{ $torneoNavida->titulo}}</h1>
                    {!! $torneoNavida->texto !!}
                @endforeach
                <h1>Fotos Del Evento</h1>
                <br>
            </div>

            <div class="card-columns">
                <!-- Bucle y pagination -->
                @foreach ($galeriaNavidad as $galeriaNavida)
                    <div class="card">
                        <a href="" data-toggle="modal" data-target="{{ '#T'.$galeriaNavida->id}}"><img src="{{ asset('storage/'.$galeriaNavida->image->url) }}" alt="{{$galeriaNavida->nombre}}" class="card-img-top"></a>
                    </div>
                @endforeach

            </div>
            <div class="col-lg-12">
                {{ $galeriaNavidad->links() }}
            </div>
        </div>
    </div>
</section>

<!-- Ventana Modal de las Imagenes-->
<!-- bucle Para que cuando hagas click en una imagen te aparesca la ventana modal con la imagen -->
@foreach ($galeriaNavidad as $galeriaNavida)
    <div class="modal fade" id="{{ 'T'.$galeriaNavida->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <img src="{{ asset('storage/'.$galeriaNavida->image->url) }}" alt="{{ $galeriaNavida->nombre}}" class="img-fluid rounded">
        </div>
    </div>
@endforeach

@endsection