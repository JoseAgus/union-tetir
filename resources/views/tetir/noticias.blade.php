@extends('layouts.master2')

@section('content')
<section class="pt-5">
    <div class="container pt-5">
        <div class="row mb-5">
            <div class="col-lg-12">
                <div class="text-center">
                    <h1>Noticias</h1>
                    <br>
                    <br>
                </div>
                <div class="row text-center">
                    {{-- Bucle con las Noticias con pagination --}}
                    {{-- Codigo para el bucle --}}
                    @foreach ($noticias as $noticia)
                        <div class="col-lg-6 mb-5">
                            <a href="{{ url('/noticia/' . $noticia->id)}}">
                                @if ($noticia->image)
                                <img class="img-fluid mb-4 h-50" src="{{ asset('storage/'.$noticia->image->url) }}" alt="" />
                                @else
                                <img class="img-fluid mb-4 h-50" src="https://pixabay.com/get/g5db51d03683eacf82f6f744d45f69ef81896738da7bf87ef2b687f4c82d00fecec291e5032c5f001d8432951a0e9c536_1280.jpg" alt="" />
                                @endif
                                
                            </a>
                            <ul class="list-inline small text-uppercase mb-0">
                            </ul>
                            <h3 class="h4 mt-2"> <a class="reset-anchor" href="{{ url('/noticia/' . $noticia->id)}}">{{ $noticia->titulo}}</a></h3>
                            <p class="text-small mb-1">{{ $noticia->descripcion}}</p><a class="btn btn-link" href="{{ url('/noticia/' . $noticia->id)}}">Continua leyendo</a>
                        </div>
                    @endforeach
                    {{-- Fin Codigo para el bucle --}}
                </div>
                <!-- Listing navigation -->
                {{ $noticias->links() }}
            </div>
        </div>
    </div>
</section>
@endsection