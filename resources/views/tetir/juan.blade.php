@extends('layouts.master2')

@section('content')
<section>
    <div class="container">
        <div class="row">
            <div id="contenedorH" class="col-lg-12 mb-5 mb-lg-0">
                <h1>Biografía de Domingo Juan Vera Alonso</h1>
                <br>
                <p>Domingo Juan Vera Alonso (Vega de Tetir, 06/05/1963) fue el pionero de una serie de futbolistas tetireños de reconocido prestigio deportivo, algunos de ellos integrantes de su propia familia, como su hermano Pedro José y sus sobrinos
                    Soto Vera.
                </p>
                <p>Domingo Juan tuvo una larga trayectoria por numerosos equipos de la isla, comenzando a hacer sus primeras habilidades con el balón en el antiguo campo de Tetir, que por aquel entonces era de tierra y ni siquiera contaba con porterías.
                    A nivel federativo jugó en el equipo de su querido pueblo, y en casi todos los clubs más señeros de Fuerteventura: La Matilla, Unión Puerto, Peña de la Amistad, Villaverde, C. D. Corralejo, U. D. Fuerteventura, Atlético Pájara,
                    C.D. La Oliva, Tarajalejo y, por último, en el C.D. Jarugo.
                </p>
                <p>Considerado uno de los mejores delanteros que ha dado la isla, estaba dotado de un gran olfato goleador, rapidez en la ejecución y con un excelente posicionamiento en el campo. Jugó en categoría Regional Preferente, llegando a tener
                    ofertas de equipos superiores que no cristalizaron por diversos motivos.
                </p>
                <p>Siendo relevante su faceta deportiva, todos los que tuvimos la suerte de conocerle podemos afirmar que eran aún superiores sus gestos humanos, su bondad y su espíritu deportivo, dejando una gran huella por todos los clubs por dónde
                    pasó. Respetado por todo el colectivo futbolístico de la isla: directivos, jugadores y árbitros, éstos últimos le obsequiaron con el Trofeo a la Máxima Deportividad, poniendo de manifiesto unos valores que ya eran públicos y notorios.
                </p>
                <p>Aunque ya no está entre nosotros, su ejemplo y espíritu de lucha permanecerán siempre en nuestra memoria.</p>
                <div class="col-lg-12 text-center">
                    <img class="w-50 py-3" src="img/Biografia/1_B.jpg" alt="Esta imagen no se carga">
                </div>
            </div>
        </div>
    </div>
</section>
@endsection