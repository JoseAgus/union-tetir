@extends('layouts.master2')

@section('content')
<section>
    <img class="w-100 py-3" src="img/Satelite.png" alt="CampusTetir 2019">
    <div class="container">
        <div class="row py-2">
            <div class="col-lg-8 mb-4 mb-lg-0">
                <h1>Contáctenos</h1>
                <br>
                <p>Estamos en: Diseminado Tetir, 109, 35613 Puerto del Rosario, Las Palmas</p>
                <p>Tel: 629 41 34 76</p>
                <p>Email: uniontetircf@gmail.com</p>
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3505.2804981178815!2d-13.93901849767278!3d28.531287509542654!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xc47b7bfdd307379%3A0x619b68c18898a729!2sDomingo%20Juan%20Vera%20Alonso!5e0!3m2!1ses!2ses!4v1617897962679!5m2!1ses!2ses"
                    width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

            </div>

            <div class="col-lg-4">
                <div class="mb-5 ">
                    <h1>Contacta Con Nosotros</h1>
                    <br>
                    @if (session('create'))
                        <div class="alert alert-success">
                            <strong>{{ session('create')}}</strong>
                        </div>
                    @endif
                    <br>
                    {!! Form::open(['route' => 'contacto.store', 'class' => 'form-register']) !!}
                        <div class="form-group">
                            {!! Form::label('nombre', 'Nombre') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-user"></i>
                                    </div>
                                </div>
                                {!! Form::text('nombre', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Nombre']) !!}
                            </div>
                            @error('nombre')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'Correo Electronico') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">@</div>
                                </div>
                                {!! Form::text('email', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Correo Electronico']) !!}
                            </div>
                            @error('email')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('mensaje', 'Mensaje') !!}
                            {!! Form::textarea('mensaje', null, [ 'class' => 'form-control', 'cols' => '40', 'rows' => '5']) !!}
                            @error('mensaje')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</section>  
@endsection