@extends('layouts.master2')

@section('content')
<section>
    <div class="container">
        <div class="row">

            <div class="col-lg-8 mb-4 mb-lg-0">
                <br>
                @if (session('create'))
                    <div class="alert alert-success">
                        <strong>{{ session('create')}}</strong>
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Hay Algún error en el Formulario Porfavor Reviselo</strong>
                    </div>
                @endif
                <br>
                <h1>Ficha Jugador</h1>
                    
                {!! Form::open(['route' => 'fichasjugador.store', 'files' => true, 'class' => 'form-register']) !!}
                    <div class="form-register__header">
                        <ul class="progressbar">
                            <li class="progressbar__option active">Ficha Jugador</li>
                            <li class="progressbar__option">Dato Tutor</li>
                        </ul>
                        
                    </div>

                    {{-- Paso1 --}}
                    <br>
                    <div class="step active" id="step-1">
                        <div class="form-group">
                            {!! Form::label('nombre', 'Nombre') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-user"></i>
                                    </div>
                                </div>
                                {!! Form::text('nombre', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Nombre']) !!}
                            </div>
                            @error('nombre')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('apellidos', 'Apellidos') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-user"></i>
                                    </div>
                                </div>
                                {!! Form::text('apellidos', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca sus Apellidos']) !!}
                            </div>
                            @error('apellidos')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('dni', 'DNI') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-address-card"></i>
                                    </div>
                                </div>
                                {!! Form::text('dni', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su DNI']) !!}
                            </div>
                            @error('dni')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('talla_camisa_polo', 'Talla Camisa o Polo') !!}
                            <div>
                                {!! Form::select('talla_camisa_polo', $tallaCamisa, null, ['class' => 'custom-select']) !!}
                            </div>
                            @error('talla_camisa_polo')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('talla_pantalon', 'Talla Pantalon') !!}

                            <div>
                                {!! Form::select('talla_pantalon', $tallaPantalon, null, ['class' => 'custom-select']) !!}
                            </div>
                            @error('talla_pantalon')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('licencia', 'Licencia') !!}

                            <div>
                                {!! Form::select('licencia', $licencia, null, ['class' => 'custom-select']) !!}
                            </div>
                            @error('licencia')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('fecha_nacimiento', 'Fecha de Nacimiento') !!}
                            <div class="input-group text-center">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                                {!! Form::date('fecha_nacimiento', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Fecha de Nacimiento']) !!}
                            </div>
                            @error('fecha_nacimiento')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('nacionalidad', 'Nacionalidad') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-flag"></i>
                                    </div>
                                </div>
                                {!! Form::text('nacionalidad', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Nacionalidad']) !!}
                            </div>
                            @error('nacionalidad')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('club_anterior', 'Club Procedencia Temporada Anterior') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-futbol"></i>
                                    </div>
                                </div>
                                {!! Form::text('club_anterior', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Club Procedencia Temporada Anterior']) !!}
                            </div>
                            @error('club_anterior')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('domicilio', 'Domicilio') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-home"></i>
                                    </div>
                                </div>
                                {!! Form::text('domicilio', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Domicilio']) !!}
                            </div>
                            @error('domicilio')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('localidad', 'Localidad') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-home"></i>
                                    </div>
                                </div>
                                {!! Form::text('localidad', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Localidad']) !!}
                            </div>
                            @error('localidad')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('codigo_postal', 'Codigo Postal') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-barcode"></i>
                                    </div>
                                </div>
                                {!! Form::text('codigo_postal', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Codigo Postal']) !!}
                            </div>
                            @error('codigo_postal')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('salud', 'Intolerancia del Jugador a algun Alimento u otras Cuestiones de Salud') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-universal-access"></i>
                                    </div>
                                </div>
                                {!! Form::text('salud', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Intolerancia del Jugador a algun Alimento u otras Cuestiones de Salud']) !!}
                            </div>
                            @error('salud')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('file', 'Dni escaneado') !!}
                            {!! Form::file('file', ['class' => 'step__input']) !!}
                            <span id="text9HelpBlock" class="form-text text-muted">Esto no sera asi se añadira un boton para poder añadir la imagen escaneada</span>

                            @error('file')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        </div>

                        <div class="form-group">
                            <button type="button" class="btn btn-primary step__button--next" data-to_step="2" data-step="1">Siguiente</button>
                        </div>
                    </div>

                    {{-- Paso2 --}}
                    <br>
                    <div class="step to-left" id="step-2">
                        <h1>Datos de Padre, Madre o Tutor si es el Jugador Menor de Edad</h1>
                        <br>
                        <p>
                            (Datos necesarios. Tienen que ser de la misma persona que vaya a firmar la ficha federativa del menor)
                        </p>
                        <div class="form-group">
                            {!! Form::label('nombre_fj', 'Nombre') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-user"></i>
                                    </div>
                                </div>
                                {!! Form::text('nombre_fj', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Nombre']) !!}
                            </div>
                            @error('nombre_fj')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('apellidos_fj', 'Apellidos') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-user"></i>
                                    </div>
                                </div>
                                {!! Form::text('apellidos_fj', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca sus Apellidos']) !!}
                            </div>
                            @error('apellidos_fj')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('dni_fj', 'DNI') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-address-card"></i>
                                    </div>
                                </div>
                                {!! Form::text('dni_fj', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su DNI']) !!}
                            </div>
                            @error('dni_fj')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('pais_nacimiento_fj', 'Pais de Nacimiento') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-home"></i>
                                    </div>
                                </div>
                                {!! Form::text('pais_nacimiento_fj', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Pais de Nacimiento']) !!}
                            </div>
                            @error('pais_nacimiento_fj')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('fecha_nacimiento_fj', 'Fecha de Nacimiento') !!}
                            <div class="input-group text-center">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                                {!! Form::date('fecha_nacimiento_fj', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Fecha de Nacimiento']) !!}
                            </div>
                            @error('fecha_nacimiento_fj')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('nacionalidad_fj', 'Nacionalidad') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-flag"></i>
                                    </div>
                                </div>
                                {!! Form::text('nacionalidad_fj', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Nacionalidad']) !!}
                            </div>
                            @error('nacionalidad_fj')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('movil_fj', 'Movil') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                </div>
                                {!! Form::text('movil_fj', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Movil']) !!}
                            </div>
                            @error('movil_fj')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('email_fj', 'Correo Electronico') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">@</div>
                                </div>
                                {!! Form::text('email_fj', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Correo Electronico']) !!}
                            </div>
                            @error('email_fj')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <button type="button" class="btn btn-primary step__button--back" data-to_step="1" data-step="2">Regresar</button>
                            {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>

            <div class="col-lg-4">
                <!--  Se incluye  -->
                <div class="mb-5 text-center">
                    <br>
                    <h3 class="h5"><i class="fa fa-exclamation-triangle"></i> Nota importante</h3>
                    <ul class="list-inline mb-0" style="background: rgba(255, 217, 0, 0.54)">
                        <li class="mb-3 p-2 text-uppercase small">
                            <p class="category-title">El importe de la tasa a pagar por jugador es de 120 euros, mediante ingreso en la C/C de la CAJARURAL nº <b>ES78 3058 6118 2227 2001 7199</b>, cuyo titular es el Unión Tetir, C.F.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</section>
@endsection