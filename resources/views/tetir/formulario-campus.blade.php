@extends('layouts.master2')

@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mb-4 mb-lg-0">
                <br>
                @if (session('create'))
                    <div class="alert alert-success">
                        <strong>{{ session('create')}}</strong>
                    </div>
                @endif
                
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Hay Algún error en el Formulario Porfavor Reviselo</strong>
                    </div>
                @endif
                <br>
                <h1>Formulario de Inscripción al Campus</h1>
                <br>
                
                
                {!! Form::open(['route' => 'inscripcionescampus.store', 'files' => true, 'class' => 'form-register']) !!}

                    <div class="form-register__header">
                        <ul class="progressbar">
                            <li class="progressbar__option active">Inscripción Campus</li>
                            <li class="progressbar__option">Dato Tutor</li>
                            <li class="progressbar__option">Dato Tutor</li>

                        </ul>
                        
                    </div>
                    <br>
                    <div class="step active" id="step-1">
                        <div class="form-group">
                            {!! Form::label('dni', 'DNI') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-address-card"></i>
                                    </div>
                                </div>
                                {!! Form::text('dni', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su DNI']) !!}
                            </div>
                            @error('dni')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! Form::label('nombre', 'Nombre') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-user"></i>
                                    </div>
                                </div>
                                {!! Form::text('nombre', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Nombre']) !!}
                            </div>
                            @error('nombre')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! Form::label('apellidos', 'Apellidos') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-user"></i>
                                    </div>
                                </div>
                                {!! Form::text('apellidos', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca sus Apellidos']) !!}
                            </div>
                            @error('apellidos')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! Form::label('fecha_nacimiento', 'Fecha de Nacimiento') !!}
                            <div class="input-group text-center">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                                {!! Form::date('fecha_nacimiento', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Fecha de Nacimiento']) !!}
                            </div>
                            @error('fecha_nacimiento')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! Form::label('domicilio', 'Domicilio') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-home"></i>
                                    </div>
                                </div>
                                {!! Form::text('domicilio', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Domicilio']) !!}
                            </div>
                            @error('domicilio')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! Form::label('club_perteneciente', 'Club / Escuaela a la que pertenece') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-futbol"></i>
                                    </div>
                                </div>
                                {!! Form::text('club_perteneciente', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Club / Escuaela a la que pertenece']) !!}
                            </div>
                            @error('club_perteneciente')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! Form::label('talla', 'Talla niño/a') !!}
                            <div>
                                {!! Form::select('talla', $talla, null, ['class' => 'custom-select']) !!}
                                <span id="TallaHelpBlock" class="form-text text-muted">Seleccione la Talla Correspondiente</span>
                            </div>
                            @error('talla')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! Form::label('categoria', 'Categoria') !!}
                            <div>
                                {!! Form::select('categoria', $categoria, null, ['class' => 'custom-select']) !!}
                            </div>
                            @error('categoria')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! Form::label('posicion_juego', 'Posicion de Juego') !!}
                            <div>
                                {!! Form::select('posicion_juego', $posicion_juego, null, ['class' => 'custom-select']) !!}
                            </div>
                            @error('posicion_juego')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <p class="font-weight-bold">Sexo</p>
             
                            <label class="mr-3">
                                {!! Form::radio('sexo', 'varon') !!}
                                Varón
                            </label>
                            <label class="mr-3">
                                {!! Form::radio('sexo', 'mujer') !!}
                                Mujer
                            </label>
             
                            @error('sexo')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
             
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-primary step__button--next" data-to_step="2" data-step="1">Siguiente</button>
                        </div>

                    </div>
                    {{-- Paso 2 --}}
                    <br>
                    <div class="step to-left" id="step-2">
                        <h1>Datos de los Tutores</h1>
                        <br>

                        <div class="form-group">
                            {!! Form::label('dni_0', 'DNI') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-address-card"></i>
                                    </div>
                                </div>
                                {!! Form::text('dni_0', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su DNI']) !!}
                            </div>
                            @error('dni_0')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! Form::label('nombre_0', 'Nombre') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-user"></i>
                                    </div>
                                </div>
                                {!! Form::text('nombre_0', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Nombre']) !!}
                            </div>
                            @error('nombre_0')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! Form::label('apellidos_0', 'Apellidos') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-user"></i>
                                    </div>
                                </div>
                                {!! Form::text('apellidos_0', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca sus Apellidos']) !!}
                            </div>
                            @error('apellidos_0')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('direccion_0', 'Dirección') !!}

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-home"></i>
                                    </div>
                                </div>
                                {!! Form::text('direccion_0', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Dirección']) !!}
                            </div>
                            @error('direccion_0')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! Form::label('localidad_0', 'Localidad') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-home"></i>
                                    </div>
                                </div>
                                {!! Form::text('localidad_0', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Localidad']) !!}
                            </div>
                            @error('localidad_0')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! Form::label('email_0', 'Correo Electronico') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">@</div>
                                </div>
                                {!! Form::text('email_0', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Correo Electronico']) !!}
                            </div>
                            @error('email_0')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! Form::label('telf_trabajo_0', 'Teléfono del Trabajo') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-phone-square"></i>
                                    </div>
                                </div>
                                {!! Form::text('telf_trabajo_0', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca el telefono de su trabajo']) !!}
                            </div>
                            @error('telf_trabajo_0')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('movil_0', 'Movil') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                </div>
                                {!! Form::text('movil_0', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Movil']) !!}
                            </div>
                            @error('movil_0')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('parentesco_0', 'Relacion de Parentesco') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-users"></i>
                                    </div>
                                </div>
                                {!! Form::text('parentesco_0', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Parentesco']) !!}
                            </div>
                            @error('parentesco_0')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-primary step__button--back" data-to_step="1" data-step="2">Regresar</button>
                            <button type="button" class="btn btn-primary step__button--next" data-to_step="3" data-step="2">Siguiente</button>
                        </div>
                        <br>
                    </div>
                    {{-- Paso 3 --}}
                    <div class="step to-left" id="step-3">
                        <div class="form-group">
                            {!! Form::label('dni_1', 'DNI') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-address-card"></i>
                                    </div>
                                </div>
                                {!! Form::text('dni_1', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su DNI']) !!}
                            </div>
                            @error('dni_1')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! Form::label('nombre_1', 'Nombre') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-user"></i>
                                    </div>
                                </div>
                                {!! Form::text('nombre_1', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Nombre']) !!}
                            </div>
                            @error('nombre_1')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! Form::label('apellidos_1', 'Apellidos') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-user"></i>
                                    </div>
                                </div>
                                {!! Form::text('apellidos_1', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca sus Apellidos']) !!}
                            </div>
                            @error('apellidos_1')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('direccion_1', 'Dirección') !!}

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-home"></i>
                                    </div>
                                </div>
                                {!! Form::text('direccion_1', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Dirección']) !!}
                            </div>
                            @error('direccion_1')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! Form::label('localidad_1', 'Localidad') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-home"></i>
                                    </div>
                                </div>
                                {!! Form::text('localidad_1', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Localidad']) !!}
                            </div>
                            @error('localidad_1')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! Form::label('email_1', 'Correo Electronico') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">@</div>
                                </div>
                                {!! Form::text('email_1', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Correo Electronico']) !!}
                            </div>
                            @error('email_1')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            {!! Form::label('telf_trabajo_1', 'Teléfono del Trabajo') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-phone-square"></i>
                                    </div>
                                </div>
                                {!! Form::text('telf_trabajo_1', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca el telefono de su trabajo']) !!}
                            </div>
                            @error('telf_trabajo_1')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('movil_1', 'Movil') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-phone"></i>
                                    </div>
                                </div>
                                {!! Form::text('movil_1', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Movil']) !!}
                            </div>
                            @error('movil_1')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            {!! Form::label('parentesco_1', 'Relacion de Parentesco') !!}
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-users"></i>
                                    </div>
                                </div>
                                {!! Form::text('parentesco_1', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca su Parentesco']) !!}
                            </div>
                            @error('parentesco_1')
                                <span class="text-danger" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <button type="button" class="btn btn-primary step__button--back" data-to_step="2" data-step="3">Regresar</button>
                            {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="col-lg-4">
                <!--  Se incluye  -->
                <div class="mb-5 text-center">
                    <br>
                    <h3 class="h5"><i class="fa fa-exclamation-triangle"></i> Nota importante</h3>
                    <ul class="list-inline mb-0" style="background: rgba(255, 217, 0, 0.54)">
                        <li class="mb-3 p-2 text-uppercase small">
                            <p class="category-title"><i class="fa fa-caret-right" aria-hidden="true"></i> Entregar este formulario cumplimentado junto con el justificante de pago en la oficina del club o enviar por correo electrónico. Así mismo deberá firmar y entregar la
                                autorización adjunta.</p>
                        </li>
                    </ul>
                </div>
                <!-- Descargas -->
                <div class="mb-5 text-center">
                    <h3 class="h5"><i class="fa fa-download" aria-hidden="true"></i> Descargas</h3>
                    <ul class="list-inline mb-0" style="background: rgba(255, 217, 0, 0.54)">
                        <li class="mb-3 p-2 text-uppercase small">
                            <p class="category-title"> <a href="descargas/Autorizacion-Campus-Tetir-2019-v1.pdf"><i
                                                            class="fa fa-caret-right" aria-hidden="true"></i> Autorizacion Campus Tetir
                                                        2019 v1</a> </p>
                        </li>
                        <li class="mb-3 p-2 text-uppercase small">
                            <p class="category-title"> <a href="descargas/Autorizacion-Campus-Tetir-2019-v2.pdf"><i
                                                            class="fa fa-caret-right" aria-hidden="true"></i> Autorizacion Campus Tetir
                                                        2019 v2</a> </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</section>
@endsection