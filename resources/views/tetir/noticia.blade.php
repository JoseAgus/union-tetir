@extends('layouts.master2')

@section('content')
  
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mb-5 mb-lg-0 text-center">
                        {{-- Cambiar la img --}}
                        @if ($noticia->image)
                        <img class="w-100 py-3 h-55" src="{{ asset('storage/'.$noticia->image->url) }}" alt="{{ $noticia->titulo }}">
                        @else
                        <img class="w-100 py-3 h-55" src="https://pixabay.com/get/g5db51d03683eacf82f6f744d45f69ef81896738da7bf87ef2b687f4c82d00fecec291e5032c5f001d8432951a0e9c536_1280.jpg" alt="{{ $noticia->titulo }}">
                        @endif
                        <div class="col-lg-12 text-center">
                            <h1>{{ $noticia->titulo }}</h1>
                        </div>
                        {!! $noticia->texto !!}
                        <br>
                        <br>
                </div>
            </div>
        </div>
    </section>

@endsection