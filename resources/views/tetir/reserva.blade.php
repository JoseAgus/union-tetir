@extends('layouts.master2')

@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mb-4 mb-lg-0">
                <br>
                @if (session('create'))
                    <div class="alert alert-success">
                        <strong>{{ session('create')}}</strong>
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Hay Algún error en el Formulario Porfavor Reviselo</strong>
                    </div>
                @endif
                <br>
                {{-- Poner el titulo  --}}
                @foreach ($reserva as $reser)

                    <h1>{{ $reser->titulo}}</h1>
                    {{-- Cambiar el texto por {!!  !!} --}}
                    {!! $reser->texto !!}

                @endforeach
                <br>
                <h1>Reserva</h1>
                <br>

                {!! Form::open(['route' => 'reservas.store', 'files' => true]) !!}
                    @csrf
                    {{-- {!! Form::hidden('user_id', user()) !!} --}}
                    <div class="form-group">
                        {!! Form::label('nombre', 'Nombre y apellidos del niño/a - joven') !!}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-user"></i>
                                </div>
                            </div>
                            {!! Form::text('nombre', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca Nombre y apellidos']) !!}
                        </div>
                        @error('nombre')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        {!! Form::label('telf_tutor', 'Teléfono Tutor') !!}
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-phone"></i>
                                </div>
                            </div>
                            {!! Form::text('telf_tutor', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca Teléfono']) !!}
                        </div>
                        @error('telf_tutor')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        {!! Form::label('fecha_nacimiento', 'Fecha de Nacimiento') !!}
                        <div class="input-group text-center">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class="fa fa-calendar"></i>
                                </div>
                            </div>
                            {!! Form::date('fecha_nacimiento', null, [ 'class' => 'form-control step__input', 'placeholder' => 'Introduzca Fecha de Nacimiento']) !!}
                        </div>
                        @error('fecha_nacimiento')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        {!! Form::label('obsevaciones', 'Observaciones') !!}
                        {!! Form::textarea('obsevaciones', null, [ 'class' => 'form-control step__input']) !!}
                        @error('obsevaciones')
                        <span class="text-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>

                    <div class="form-group">
                        {!! Form::label('file', 'Dni escaneado') !!}
                        {!! Form::file('file', ['class' => 'step__input']) !!}
                        @error('file')
                            <span class="text-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
                    
                {!! Form::close() !!}

                <br>

            </div>
            <div class="col-lg-4">
                <!--  Se incluye  -->
                <div class="mb-5 text-center">
                    <br>
                    <h3 class="h5"><i class="fa fa-exclamation-triangle"></i> Nota importante</h3>
                    <ul class="list-inline mb-0" style="background: rgba(255, 217, 0, 0.54)">
                        <li class="mb-3 p-2 text-uppercase small">
                            <p class="category-title">Cuenta Corriente-Cajarural-Cajamar: <b>ES78 3058 6118 2227 20017199</b> </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

</section>
@endsection