@extends('layouts.master2')

@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mb-4 mb-lg-0">
                <!-- Sustituirlo en blade por -->
                {{-- Poner {!! el texto !!} --}}
                <div class="my-3 float-left">
                    <div class="col-lg-12 text-center">
                        {{-- Poner el Titulo por la BD --}}
                        {{-- <h1>V Campus de Fútbol Tetir 2019</h1> --}}
                        @foreach ($infoCampus as $infoCampu)
                            <h1>{{ $infoCampu->titulo }}</h1>
                        @endforeach
                    </div>
                    {{-- <p class="mb-4">Un año más, el Unión Tetir CF organiza el Campus de fútbol destinado a jóvenes de 5 a 16 años, cuyo desarrollo tendrá lugar en la instalación deportiva Domingo J. Vera Alonso, de Tetir, desde el 29 de junio al 6 de julio.</p> --}}
                    <img class="img-fluid mb-4 float-left mr-2" src="img/uniontetir_campo.jpg" alt="Campo Tetir">
                    @foreach ($infoCampus as $infoCampu)
                        {!! $infoCampu->texto !!}
                    @endforeach
                    
                    {{-- <p class="mb-5">Para la inscripción debes descargarte los tres PDFs listados a continuación. Una vez descargados se deben abrir en el Adobe Reader o similar. En el caso de no tenerlo instalado lo puedes descargar haciendo <a href="https://get.adobe.com/es/reader/otherversions/">click
                        aquí</a>.</p>

                    <p class="mb-5">Se deben rellenar los tres PDFs, guardarlos y enviarlos por email junto con el resguardo del pago de la inscripción a la dirección <a href="mailto:uniontetircf@hotmail.com">uniontetircf@hotmail.com</a>.</p>
                    <p class="mb-5">El pago de la inscripción debe realizarse en Cajamar (antigua Caja Rural), frente al Ayuntamiento de Puerto del Rosario. La cuenta es: <b>ES78 3058 6118 2227 20017199</b>.</p>

                    <p class="mb-5">Para solventar cualquier duda o aclaración, puede llamarnos a los siguientes teléfonos: <b>629 413 476</b> y <b>636 476 777</b>.</p> --}}

                </div>

                <!-- Boton Para Inscribirse -->
                <div class="col-lg-12 text-center">
                    <a href="/formulario-campus" type="button" class="btn btn-primary">Inscribete ya</a>
                </div>
                <br>
                <br>
                
            </div>


            <div class="col-lg-4">
                <!--  Se incluye  -->
                <div class="mb-5 text-center">

                    <h3 class="h5"><i class="fa fa-check" aria-hidden="true"></i> Se incluye</h3>
                    <ul class="list-inline mb-0" style="background: rgba(255, 217, 0, 0.54)">
                        <li class="mb-3 p-2 text-uppercase small">
                            <p class="category-title"><i class="fa fa-caret-right" aria-hidden="true"></i> Dos equipaciones de juego</p>
                        </li>
                        <li class="mb-3 p-2 text-uppercase small">
                            <p class="category-title"><i class="fa fa-caret-right" aria-hidden="true"></i> Desayuno: bocadillo, zumo y fruta</p>
                        </li>
                        <li class="mb-3 p-2 text-uppercase small">
                            <p class="category-title"><i class="fa fa-caret-right" aria-hidden="true"></i> Diploma del Campus</p>
                        </li>
                        <li class="mb-3 p-2 text-uppercase small">
                            <p class="category-title"><i class="fa fa-caret-right" aria-hidden="true"></i> Ficha individual de evaluación</p>
                        </li>
                        <li class="mb-3 p-2 text-uppercase small">
                            <p class="category-title"><i class="fa fa-caret-right" aria-hidden="true"></i> Seguro de accidente y RC</p>
                        </li>
                        <li class="mb-3 p-2 text-uppercase small">
                            <p class="category-title"><i class="fa fa-caret-right" aria-hidden="true"></i> Visita de ojeadores de equipos nacionales</p>
                        </li>
                        <li class="mb-3 p-2 text-uppercase small">
                            <p class="category-title"><i class="fa fa-caret-right" aria-hidden="true"></i> Visita de jugadores de 1º División</p>
                        </li>
                        <li class="mb-3 p-2 text-uppercase small">
                            <p class="category-title"><i class="fa fa-caret-right" aria-hidden="true"></i> Fotografías
                            </p>
                        </li>
                    </ul>
                </div>
                <!-- No se incluye -->
                <div class="mb-5 text-center">
                    <h3 class="h5"><i class="fa fa-times" aria-hidden="true"></i> No se incluye</h3>
                    <ul class="list-inline mb-0" style="background: rgba(255, 217, 0, 0.54)">
                        <li class="mb-3 p-2 text-uppercase small">
                            <p class="category-title"><i class="fa fa-caret-right" aria-hidden="true"></i> Botas y medias
                            </p>
                        </li>
                        <li class="mb-3 p-2 text-uppercase small">
                            <p class="category-title"><i class="fa fa-caret-right" aria-hidden="true"></i> Objetos de aseo personal</p>
                        </li>
                        <li class="mb-3 p-2 text-uppercase small">
                            <p class="h5 category-title">¡¡PLAZAS LIMITADAS!!</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection