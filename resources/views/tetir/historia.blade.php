@extends('layouts.master2')

@section('content')
<section>
    <div class="container">
        <div class="row">
            <div id="contenedorH" class="col-lg-12 mb-5 mb-lg-0">
                <h1>Historia Del Unión Tetir CF</h1>
                <br>
                <p>La Asociación Deportiva La Vega de Tetir (Unión Tetir, CF) es un equipo de fútbol base que desarrolla su actividad en el pueblo de Tetir, isla de Fuerteventura, Canarias.</p>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7010.5546520827775!2d-13.937522724567021!3d28.531382879831085!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xc47b7ea0ae91db9%3A0x2248be8b9a03b7db!2s35613%20Tetir%2C%20Las%20Palmas!5e0!3m2!1ses!2ses!4v1617640633454!5m2!1ses!2ses"
                    width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                <br>
                <br>
                <p>Tetir es una población de unos 1.000 habitantes, cabeza de una parroquia de unos 2.000 vecinos. El pueblo fue municipio hasta el año 1925, aunque actualmente pertenece a Puerto del Rosario, capital de la isla, que se encuentra a una
                    distancia de 8 kilómetros de Tetir. </p>
                <br>
                <p>El origen del equipo local se remonta a las fiestas patronales en Honor a Santo Domingo del año 1969 y posteriormente fue participando en las distintas competiciones oficiales de Fuerteventura.
                </p>
                <img class="w-100 py-3" src="img/Historia/2_H.jpg" alt="Esta imagen no se carga">
                <br>
                <br>
                <p>Durante los años ochenta del siglo pasado el club vuelve a crear un equipo para la liga regional y mantiene su participación en las competiciones de futbol base. En esta etapa nuestros equipos de cantera consiguen algunos éxitos deportivos
                    de carácter insular.
                </p>
                <div class="col-lg-12 text-center">
                    <img class="py-3" src="img/Historia/3_H.jpg" alt="Esta imagen no se carga" style="width: 35%; height: 10%;">
                </div>
                <br>
                <br>
                <p>El actual Unión Tetir Club de Fútbol es una entidad deportiva fundada en el año 1993, después de permanecer algunos años fuera de competición. En la década de los noventa adquiere protagonismo el equipo de categoría cadete, con la
                    participación en competiciones de carácter provincial.
                </p>
                <p>Reapareció de nuevo en el año 2013 para trabajar con el futbol base, nutriéndose de niños y niñas de la localidad y de los pueblos aledaños.</p>
                <br>
                <br>
                <br>
                <br>
                <br>
                <p>La entidad tiene como finalidad primordial la de ofrecer una alternativa deportiva a los niños/as y jóvenes de Tetir y pueblos aledaños (La Asomada, Los Estancos, El Time, Casillas del Ángel, Tesjuates, La Matilla y Tefía). Nuestro
                    empeño es facilitar a los jóvenes de la comarca la participación en una actividad que genera integración, dinamización social y hábitos de vida saludables.
                </p>
                <p>En la temporada 2015-16, nuestra excelente cantera nos ha vuelto a dar una alegría deportiva. El Benjamín “B” ha obtenido el campeonato de Liga en su grupo, y posteriormente, la Superliga insular, es decir, el campeonato de Fuerteventura
                    de la categoría benjamín.
                </p>
                <div class="col-lg-12 text-center">
                    <img class="w-50 py-3" src="img/Historia/5_H.jpg" alt="Esta imagen no se carga">
                </div>
                <br>
                <br>
                <p>Nuestra joven cantera no para de darnos satisfacciones deportivas. En la temporada 2016-17, nuestros jugadores del Benjamín “A” se proclamaron capeones de Copa Grupo 1, y posteriormente Campeón Insular Absoluto de Copa Benjamín.
                </p>
                <div class="col-lg-12 text-center">
                    <img class="w-50 py-3" src="img/Historia/6_H.jpg" alt="Esta imagen no se carga">
                </div>
                <br>
                <br>
                <p>El Tetir CF desarrolla su actividad en el Campo Municipal Domingo J. Vera Alonso, gestionado en exclusiva por nuestro club. Se trata de una excelente instalación de césped sintético, graderío, vestuarios, dependencias anexas y cerramiento
                    perimetral del campo.
                </p>
                <p>En la actualidad contamos con seis equipos de futbol base que participan en las categorías benjamín, alevín, infantil, cadete y juvenil. Además, el U. Tetir tiene su propia escuela de fútbol para los niños y niñas más pequeños, desde
                    los 5 a los 7 años. En total, manejamos una cifra superior a los 120 jugadores, desde los 5 hasta los 18 años.
                </p>
                <br>
            </div>
        </div>
    </div>
</section>
@endsection