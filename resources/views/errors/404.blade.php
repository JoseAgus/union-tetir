@extends('layouts.master3')

@section('content')

    <div class="col-sm-12 col-md-12" style="margin-top: 50px">
        <section class="content-padder error-404 not-found jumbotron text-center">
            <header class="page-header">
                <h1 style="font-size: 80px">404</h1>
            </header>
            <div class="page-content">
                <h2 class="entry-title"></h2>
                <p>No se ha encontrado nada en esta ubicación.</p>
                <p>Trate de volver a la <a href="/"><strong>Página de inicio</strong></a> en su lugar </p>
            </div>
        </section>
    </div>

@endsection